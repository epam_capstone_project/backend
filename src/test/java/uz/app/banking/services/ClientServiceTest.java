package uz.app.banking.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Iterator;
import java.util.Optional;
import java.util.UUID;

import org.checkerframework.checker.units.qual.m;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.ApplicationContext;

import uz.app.banking.TestConfig;
import uz.app.banking.dto.ClientSaveReqDTO;
import uz.app.banking.dto.ClientSaveRespDTO;
import uz.app.banking.entities.Client;
import uz.app.banking.mockobjects.TestUsers;
import uz.app.banking.references.Country;
import uz.app.banking.references.DocType;
import uz.app.banking.repositories.ClientRepository;
import uz.app.banking.repositories.UserRepository;
import uz.app.banking.security.controllers.AuthControllerTest;


public class ClientServiceTest {

    ApplicationContext context;
    ClientService clientService;
    UserRepository userRepo;
    ClientRepository clientRepo;

    @Before
    public void init() {
        this.context = TestConfig.getContext();
        this.userRepo = context.getBean(UserRepository.class);
        this.clientRepo = context.getBean(ClientRepository.class);
        this.clientService = context.getBean(ClientService.class);
    }

    @Test
    public void testCreatePositive() throws Exception {
        AuthControllerTest.login(context, TestUsers.admin.login, TestUsers.admin.password);
        ClientSaveReqDTO req = getClientSaveReq();
        ClientSaveRespDTO resp = this.clientService.create(req);

        Optional<Client> opClient =  clientRepo.findById(resp.getClientID());
        assertTrue(opClient.isPresent());
        assertTrue(userRepo.findByLogin(resp.getLogin()).isPresent());

        // cleanup after creation
        clientRepo.deleteById(resp.getClientID());
        userRepo.deleteById(userRepo.findByLogin(resp.getLogin()).get().getUserId());
    }

    @Test
    public void testCreateNegative() throws Exception {
        AuthControllerTest.login(context, TestUsers.admin.login, TestUsers.admin.password);
        // Check branch validation
        ClientSaveReqDTO clientWithInvalidBranch = getClientSaveReq();
        clientWithInvalidBranch.setBranch(UUID.randomUUID().toString());
        assertThrows(RuntimeException.class, () -> {
            this.clientService.create(clientWithInvalidBranch);
        });
        assertTrue(
            clientRepo.findByDocTypeAndDocSeriaAndDocNumber(
                DocType.getByCode(clientWithInvalidBranch.getDocType()),
                clientWithInvalidBranch.getDocSeria(),
                clientWithInvalidBranch.getDocNumber())
                    .stream()
                    .filter(x -> x.getBranch().getCode().equals(clientWithInvalidBranch.getBranch()))
                    .count() == 0); // ensure that no client created 

        // check creating existing client
        if(clientRepo.count() > 0) {
            ClientSaveReqDTO clientClone = getClientSaveReq();
            Client existingClient = clientRepo.findAll().iterator().next();
            clientClone.setDocType(existingClient.getDocType().getCode());
            clientClone.setDocSeria(existingClient.getDocSeria());
            clientClone.setDocNumber(existingClient.getDocNumber());
            clientClone.setBranch(existingClient.getBranch().getCode());
            assertThrows(RuntimeException.class, () -> {
                this.clientService.create(clientClone);
            });
            assertTrue(
            clientRepo.findByDocTypeAndDocSeriaAndDocNumber(
                DocType.getByCode(clientClone.getDocType()),
                clientClone.getDocSeria(),
                clientClone.getDocNumber())
                    .stream()
                    .filter(x -> x.getBranch().getCode().equals(clientClone.getBranch()))
                    .count() == 1);  // only existing client must be in DB, without clone
        }
    }

    @Test
    public void testUpdatePositive() throws Exception {
        AuthControllerTest.login(context, TestUsers.admin.login, TestUsers.admin.password);
        ClientSaveReqDTO req = getClientSaveReq();
        ClientSaveRespDTO resp = this.clientService.create(req);

        Optional<Client> opClient = clientRepo.findById(resp.getClientID());
        assertTrue(opClient.isPresent());
        assertTrue(userRepo.findByLogin(resp.getLogin()).isPresent());
        assertSameClient(req, opClient.get());

        req.setClientId(resp.getClientID());
        req.setBranch("100");
        req.setFirstName("Anne");
        req.setLastName("Marie");
        req.setMiddleName("Curie");
        req.setDocType(DocType.DRIVER_LIC.getCode());
        resp = this.clientService.create(req);

        opClient = clientRepo.findById(resp.getClientID());
        assertTrue(opClient.isPresent());
        assertTrue(userRepo.findByLogin(resp.getLogin()).isPresent());
        assertSameClient(req, opClient.get());
        
        // cleanup after creation
        clientRepo.deleteById(resp.getClientID());
        userRepo.deleteById(userRepo.findByLogin(resp.getLogin()).get().getUserId());
    }

    @Test
    public void testUpdateNegative() {
        AuthControllerTest.login(context, TestUsers.admin.login, TestUsers.admin.password);
        if(clientRepo.count() > 1) {
            Iterator<Client> clientsIter = clientRepo.findAll().iterator();
            Client existingClient = clientsIter.next();
            Client anotherExistingClient = clientsIter.next();
            anotherExistingClient.setDocType(existingClient.getDocType());
            anotherExistingClient.setDocSeria(existingClient.getDocSeria());
            anotherExistingClient.setDocNumber(existingClient.getDocNumber());
            anotherExistingClient.setBranch(existingClient.getBranch());
            assertThrows(RuntimeException.class, () -> {
                clientService.update(getClientSaveReqDTO(anotherExistingClient));
            });
        }
    }

    ClientSaveReqDTO getClientSaveReq() {
        ClientSaveReqDTO req = new ClientSaveReqDTO();
        req.setFirstName("Klyde");
        req.setLastName("Cleveland");
        req.setMiddleName("Kan");
        req.setDocType(DocType.ID_CARD.getCode());
        req.setDocSeria("ID");
        req.setDocNumber("123123456345");
        req.setBranch("000");
        req.setResidency(Country.GBR.getCode());
        return req;
    }

    ClientSaveReqDTO getClientSaveReqDTO(Client cl) {
        ClientSaveReqDTO req = new ClientSaveReqDTO();
        req.setFirstName(cl.getFirstName());
        req.setLastName(cl.getLastName());
        req.setMiddleName(cl.getMiddleName());
        req.setDocType(cl.getDocType().getCode());
        req.setDocSeria(cl.getDocSeria());
        req.setDocNumber(cl.getDocNumber());
        req.setBranch(cl.getBranch().getCode());
        req.setResidency(cl.getResidency().getCode());
        return req;
    }

    void assertSameClient(ClientSaveReqDTO req, Client cl) {
        assertEquals(req.getBranch(), cl.getBranch().getCode());
        assertEquals(req.getDocNumber(), cl.getDocNumber());
        assertEquals(req.getDocSeria(), cl.getDocSeria());
        assertEquals(req.getDocType(), cl.getDocType().getCode());
        assertEquals(req.getFirstName(), cl.getFirstName());
        assertEquals(req.getLastName(), cl.getLastName());
        assertEquals(req.getMiddleName(), cl.getMiddleName());
        assertEquals(req.getResidency(), cl.getResidency().getCode());
    }
}
