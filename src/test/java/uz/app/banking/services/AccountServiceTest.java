package uz.app.banking.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.lang.reflect.Array;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import uz.app.banking.TestConfig;
import uz.app.banking.dto.AccountCreateReqDTO;
import uz.app.banking.dto.AccountCreateRespDTO;
import uz.app.banking.dto.AccountDTO;
import uz.app.banking.entities.Account;
import uz.app.banking.mockobjects.TestClients;
import uz.app.banking.mockobjects.TestUsers;
import uz.app.banking.references.BalanceAccount;
import uz.app.banking.references.Currency;
import uz.app.banking.repositories.AccountRepository;
import uz.app.banking.security.controllers.AuthControllerTest;

public class AccountServiceTest {

    ApplicationContext context;
    AccountService accountService;
    AccountRepository accRepo;


    @Before
    public void init() {
        this.context = TestConfig.getContext();
        this.accountService = context.getBean(AccountService.class);
        this.accRepo = context.getBean(AccountRepository.class);
    }

    @Test
    public void testCreateAccPositive() {
        AuthControllerTest.login(context, TestUsers.emp.login, TestUsers.emp.password);
        AccountCreateReqDTO req = getAccountCreateRequest();
        AccountCreateRespDTO resp = this.accountService.createAcc(req);
        assertTrue(accRepo.findById(resp.getAccId()).isPresent());
    }

    @Test
    public void testCreateAccNegative() {
        AuthControllerTest.login(context, TestUsers.emp.login, TestUsers.emp.password);
        AccountCreateReqDTO req = getAccountCreateRequest();
        req.setClientId(9898989898l);
        long countBeforeAttempt = this.accRepo.count();
        assertThrows(RuntimeException.class, () -> this.accountService.createAcc(req));
        long countAfterAttempt = this.accRepo.count();
        assertEquals(countBeforeAttempt, countAfterAttempt);
    }

    @Test
    public void testGetByBalanceAccLike() {
        final String balanceAccLike = "10";
        Iterable<Account> allAccs = accRepo.findAll();
        Page<AccountDTO> filteredAccs = accountService.getByBalanceAccLike(balanceAccLike, Pageable.unpaged());
        filteredAccs
            .forEach(x-> {
                assertTrue(
                    String.format("Expected balance account like: %s; Got: %s", balanceAccLike, x.getBalanceAcc().getCode()), 
                    x.getBalanceAcc().getCode().indexOf(balanceAccLike) >= 0);
            });

        int actualCount = 0;
        for (Account account : allAccs) {
            if(account.getBalanceAcc().getCode().indexOf(balanceAccLike) >= 0) {
                actualCount++;
            }
        }
        assertEquals(filteredAccs.getTotalElements(), actualCount);
    }

    @Test
    public void testGetByCurrencyLike() {
        final String currencyLike = "8";
        Iterable<Account> allAccs = accRepo.findAll();
        Page<AccountDTO> filteredAccs = accountService.getByCurrencyLike(currencyLike, Pageable.unpaged());
        filteredAccs
            .forEach(x-> {
                assertTrue(
                    String.format("Expected balance account like: %s; Got: %s", currencyLike, x.getCurrency().getCode()), 
                    x.getCurrency().getCode().indexOf(currencyLike) >= 0);
            });

        int actualCount = 0;
        for (Account account : allAccs) {
            if(account.getCurrency().getCode().indexOf(currencyLike) >= 0) {
                actualCount++;
            }
        }
        assertEquals(filteredAccs.getTotalElements(), actualCount);
    }

    public AccountCreateReqDTO getAccountCreateRequest() {
        AccountCreateReqDTO req = new AccountCreateReqDTO();
        req.setBalanceAcc(BalanceAccount.CLIENT_DEPOSIT.getCode());
        req.setBranchCode("100");
        req.setClientId(TestClients.client_1.getId());
        req.setCurrency(Currency.USD.getCode());
        req.setFrozen(false);
        req.setNegativeBalanceEnabled(false);
        return req;
    }
}
