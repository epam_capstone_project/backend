package uz.app.banking.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

import uz.app.banking.TestConfig;
import uz.app.banking.dto.TransactInfoDTO;
import uz.app.banking.entities.Account;
import uz.app.banking.entities.Transaction;
import uz.app.banking.exceptions.FrozenAccountException;
import uz.app.banking.exceptions.IncorrectTransactStateException;
import uz.app.banking.exceptions.OverdraftException;
import uz.app.banking.mockobjects.TestAccounts;
import uz.app.banking.mockobjects.TestUsers;
import uz.app.banking.mockobjects.TestUsers.TestUser;
import uz.app.banking.references.Currency;
import uz.app.banking.references.TransactState;
import uz.app.banking.repositories.AccountRepository;
import uz.app.banking.repositories.TransactionRepository;
import uz.app.banking.security.controllers.AuthControllerTest;

public class TransactServiceTest {
    ApplicationContext context;
    TransactionRepository trRepo;
    AccountRepository accRepo;
    TransactService trService;

    @Before
    public void init() {
        this.context = TestConfig.getContext();
        this.trRepo = this.context.getBean(TransactionRepository.class);
        this.trService = this.context.getBean(TransactService.class);
        this.accRepo = this.context.getBean(AccountRepository.class);
    }

    @Test
    public void testCreatePositive() {
        AuthControllerTest.login(context, TestUsers.emp.login, TestUsers.emp.password);

        final long transactSum = 100_00l;
        final Currency transactCurrency = Currency.UZS;
        final String transactBranch = "100";
        final String creditAcc = TestAccounts.acc_101.getFullAcc();
        final String debitAcc = TestAccounts.acc_001.getFullAcc();

        // Overdraft case
        // final String creditAcc = TestAccounts.acc_001.getFullAcc();
        // final String debitAcc = TestAccounts.acc_101.getFullAcc();

        Iterable<Account> allAccs = accRepo.findAll();
        Account creditAccBeforeTransact = findAccount(creditAcc, allAccs);
        Account debitAccBeforeTransact = findAccount(debitAcc, allAccs);
        
        
        TransactInfoDTO trInfo = new TransactInfoDTO();
        //trInfo.setBranch();
        trInfo.setBranch(transactBranch);
        trInfo.setCredit(creditAcc);
        trInfo.setDebit(debitAcc);
        trInfo.setCurrency(transactCurrency);
        trInfo.setSum(transactSum); // 100 Soums
        trInfo.setPurpose("Test purpose: positive case");

        trInfo = this.trService.create(trInfo);
        Optional<Transaction> newCreatedTransact = this.trRepo.findById(trInfo.getTrId());
        assertTrue(newCreatedTransact.isPresent());
        assertEquals(newCreatedTransact.get().getCredit().getFullAcc(), creditAcc);
        assertEquals(newCreatedTransact.get().getDebit().getFullAcc(), debitAcc);
        assertEquals(newCreatedTransact.get().getCurrency(), transactCurrency);
        assertEquals(newCreatedTransact.get().getSum(), Long.valueOf(transactSum));
        assertEquals(newCreatedTransact.get().getBranch().getCode(), transactBranch);

        // Check account balances
        allAccs = accRepo.findAll();
        Account creditAccAfterTransact = findAccount(creditAcc, allAccs);
        Account debitAccAfterTransact = findAccount(debitAcc, allAccs);
        assertEquals(creditAccBeforeTransact.getAvailableBalance() - creditAccAfterTransact.getAvailableBalance(), -transactSum); // credited account must increase
        assertEquals(debitAccBeforeTransact.getAvailableBalance() - debitAccAfterTransact.getAvailableBalance(), transactSum); // debited account must decrease
        assertEquals(creditAccBeforeTransact.getActualBalance() - creditAccAfterTransact.getActualBalance(), -transactSum);
        assertEquals(debitAccBeforeTransact.getActualBalance() - debitAccAfterTransact.getActualBalance(), transactSum);
    }

    @Test
    public void testCreateNegative() {
        AuthControllerTest.login(context, TestUsers.emp.login, TestUsers.emp.password);

        final long transactSum = 100_00l;
        final Currency transactCurrency = Currency.UZS;
        final String transactBranch = "100";

        // Overdraft case
        final String creditAcc = TestAccounts.acc_001.getFullAcc();
        final String debitAcc = TestAccounts.acc_101.getFullAcc();
        final String clientFrozenAcc = TestAccounts.acc_CL_DEP.getFullAcc();

        Iterable<Account> allAccs = accRepo.findAll();
        Account creditAccBeforeTransact = findAccount(creditAcc, allAccs);
        Account debitAccBeforeTransact = findAccount(debitAcc, allAccs);
        Account clientFrozenAccBeforeTransact = findAccount(clientFrozenAcc, allAccs);
        
        
        if(!debitAccBeforeTransact.isNegativeBalanceEnabled()) {
            assertThrows(OverdraftException.class, () -> {
                TransactInfoDTO trInfo = new TransactInfoDTO();
                //trInfo.setBranch();
                trInfo.setBranch(transactBranch);
                trInfo.setCredit(creditAcc);
                trInfo.setDebit(debitAcc);
                trInfo.setCurrency(transactCurrency);
                trInfo.setSum(Long.MAX_VALUE);
                trInfo.setPurpose("Test purpose: negative case");
                this.trService.create(trInfo);
            });
        }
        if(clientFrozenAccBeforeTransact.isFrozen()) {
            assertThrows(FrozenAccountException.class, () -> {
                TransactInfoDTO trInfo = new TransactInfoDTO();
                //trInfo.setBranch();
                trInfo.setBranch(transactBranch);
                trInfo.setCredit(creditAcc);
                trInfo.setDebit(clientFrozenAcc);
                trInfo.setCurrency(transactCurrency);
                trInfo.setSum(transactSum); // 100 Soums
                trInfo.setPurpose("Test purpose: negative case");
                this.trService.create(trInfo);
            });
        }
    }


    @Test
    public void testChangeStatePositive() {
        AuthControllerTest.login(context, TestUsers.emp.login, TestUsers.emp.password);
        final Long testTransactId = 1l;
        Transaction trBefore = trRepo.findById(testTransactId).get();
        assertNotEquals(TransactState.APPROVED, trBefore.getState());
        trService.changeState(testTransactId, TransactState.APPROVED);
        Transaction trAfter = trRepo.findById(testTransactId).get();
        assertEquals(TransactState.APPROVED, trAfter.getState());
        // trService.changeState(testTransactId, trBefore.getState());
        trRepo.save(trBefore);
    }

    @Test
    public void testChangeStateNegative() {
        AuthControllerTest.login(context, TestUsers.emp.login, TestUsers.emp.password);
        final Long testTransactId = 1l;
        Transaction transact = trRepo.findById(testTransactId).get();

        
        // Check each state
        for (TransactState state : TransactState.values()) {
            transact.setState(state);
            trRepo.save(transact);
            switch(transact.getState()) {
                case WAITING_APPROVAL: {
                    // Test canceling the transaction
                    Account creditBefore = accRepo.findById(transact.getCredit().getAccId()).get();
                    Account debitBefore = accRepo.findById(transact.getDebit().getAccId()).get();
                    trService.changeState(testTransactId, TransactState.CANCELLED); // This is supposed to be terminal state. Any change after this state is inacceptable.
                    Account creditAfter = accRepo.findById(transact.getCredit().getAccId()).get();
                    Account debitAfter = accRepo.findById(transact.getDebit().getAccId()).get();
                    assertTrue(creditBefore.getAvailableBalance() - creditAfter.getAvailableBalance() == transact.getSum());
                    assertTrue(debitBefore.getAvailableBalance() - debitAfter.getAvailableBalance() == -transact.getSum());
                    assertTrue(creditBefore.getActualBalance() - creditAfter.getActualBalance() == transact.getSum());
                    assertTrue(debitBefore.getActualBalance() - debitAfter.getActualBalance() == -transact.getSum());
                    break;
                }
                case APPROVED: {
                    assertThrows(IncorrectTransactStateException.class, () -> {
                        trService.changeState(testTransactId, TransactState.NOT_APPROVED);
                    });
                    assertThrows(IncorrectTransactStateException.class, () -> {
                        trService.changeState(testTransactId, TransactState.WAITING_APPROVAL);
                    });
                    // Test canceling the transaction
                    Account creditBefore = accRepo.findById(transact.getCredit().getAccId()).get();
                    Account debitBefore = accRepo.findById(transact.getDebit().getAccId()).get();
                    trService.changeState(testTransactId, TransactState.CANCELLED);
                    Account creditAfter = accRepo.findById(transact.getCredit().getAccId()).get();
                    Account debitAfter = accRepo.findById(transact.getDebit().getAccId()).get();
                    assertTrue(creditBefore.getAvailableBalance() - creditAfter.getAvailableBalance() == transact.getSum());
                    assertTrue(debitBefore.getAvailableBalance() - debitAfter.getAvailableBalance() == -transact.getSum());
                    assertTrue(creditBefore.getActualBalance() - creditAfter.getActualBalance() == transact.getSum());
                    assertTrue(debitBefore.getActualBalance() - debitAfter.getActualBalance() == -transact.getSum());
                    break;
                }
                case CANCELLED: {
                    assertThrows(IncorrectTransactStateException.class, () -> {
                        trService.changeState(testTransactId, TransactState.APPROVED);
                    });
                    assertThrows(IncorrectTransactStateException.class, () -> {
                        trService.changeState(testTransactId, TransactState.NOT_APPROVED);
                    });
                    assertThrows(IncorrectTransactStateException.class, () -> {
                        trService.changeState(testTransactId, TransactState.WAITING_APPROVAL);
                    });
                    break;
                }
                case NOT_APPROVED: {
                    assertThrows(IncorrectTransactStateException.class, () -> {
                        trService.changeState(testTransactId, TransactState.APPROVED);
                    });
                    assertThrows(IncorrectTransactStateException.class, () -> {
                        trService.changeState(testTransactId, TransactState.WAITING_APPROVAL);
                    });
                    assertThrows(IncorrectTransactStateException.class, () -> {
                        trService.changeState(testTransactId, TransactState.CANCELLED);
                    });
                    break;
                }
                default:
                    throw new RuntimeException("Modify the test for new added states!");
            }
        }
    }

    @Test
    public void testEditPositive() {
        AuthControllerTest.login(context, TestUsers.emp.login, TestUsers.emp.password);
        final Long testTransactId = 1l;
        final String transactNewPurpose = "TEST EDIT METHOD";
        Transaction trBefore = trRepo.findById(testTransactId).get();
        assertNotEquals(transactNewPurpose, trBefore.getPurpose());
        TransactInfoDTO trInfoDto = TransactInfoDTO.toDTO(trBefore);
        trInfoDto.setPurpose(transactNewPurpose);
        trService.edit(trInfoDto);
        Transaction trAfter = trRepo.findById(testTransactId).get();
        assertEquals(transactNewPurpose, trAfter.getPurpose());
    }


    static Account findAccount(String fullAccCode, Iterable<Account> allAccs) {
        return StreamSupport.stream(allAccs.spliterator(), false)
            .filter(x -> x.getFullAcc().equals(fullAccCode))
            .findFirst().orElseThrow(() -> new NoSuchElementException("No such account found: " + fullAccCode));
    }

    Account loadAccount(String fullAccCode) {
        Iterable<Account> allAccs = accRepo.findAll();
        return findAccount(fullAccCode, allAccs);
    }
}
