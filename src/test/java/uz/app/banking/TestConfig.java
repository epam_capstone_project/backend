package uz.app.banking;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@Configuration
@ComponentScan("uz.app.banking")
@PropertySource("classpath:test.properties")
public class TestConfig {
    @Value("${spring.datasource.driver-class-name}")
    private String driverClass;

    @Value("${spring.datasource.url}")
    private String url;

    @Value("${spring.datasource.username}")
    private String userName;

    @Value("${spring.datasource.password}")
    private String password;

    private static final ApplicationContext context = new AnnotationConfigApplicationContext("uz.app.banking");

    // @Bean
    // public ApplicationContext context() {
    //     return new AnnotationConfigApplicationContext("uz.app.banking");
    // }

    public static ApplicationContext getContext() {
        return context;
    }

    @Bean
    public DataSource dataSource(){
        // EmbeddedDatabaseBuilder dBuilder = new EmbeddedDatabaseBuilder();
        // dBuilder.addScripts("classpath:db_test/schema.sql", "classpath:db_test/data.sql");
        // dBuilder.setType(EmbeddedDatabaseType.H2);
        // return dBuilder.build();
        DriverManagerDataSource source = new DriverManagerDataSource();
        source.setDriverClassName(driverClass);
        source.setUrl(url);
        source.setUsername(userName);
        source.setPassword(password);
        return source;
    }

    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate(){
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(this.dataSource());
        return namedParameterJdbcTemplate;
    }
}
