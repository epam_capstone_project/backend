package uz.app.banking.mockobjects;

public class TestClients {
    public static final TestClient client_1 = new TestClient(1l, 1l);
    public static final TestClient client_2 = new TestClient(2l, 4l);

    public static class TestClient {
        private Long id;
        private Long userId;
        
        public TestClient(Long id, Long userId) {
            this.id = id;
            this.userId = userId;
        }

        public Long getId() {
            return id;
        }

        public Long getUserId() {
            return userId;
        }
    }
}
