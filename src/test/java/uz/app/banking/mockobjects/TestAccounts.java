package uz.app.banking.mockobjects;

import uz.app.banking.references.BalanceAccount;
import uz.app.banking.references.Currency;

public class TestAccounts {
    
    public static final TestAccount acc_001 = new TestAccount(1l, "001860100000000001", "100", Currency.UZS, BalanceAccount.BANK_PROFIT);
    public static final TestAccount acc_101 = new TestAccount(2l, "101860100000000001", "100", Currency.UZS, BalanceAccount.INTERBANK_IN);
    public static final TestAccount acc_CL_DEP = new TestAccount(3l, "101860100000000001", "100", Currency.UZS, BalanceAccount.CLIENT_DEPOSIT);
    
    public static class TestAccount {
        private Long accId;
        private String fullAcc;
        private String branchCode;
        private Currency currency;
        private BalanceAccount balanceAccount;
        public TestAccount(Long accId, String fullAcc, String branchCode, Currency currency,
                BalanceAccount balanceAccount) {
            this.accId = accId;
            this.fullAcc = fullAcc;
            this.branchCode = branchCode;
            this.currency = currency;
            this.balanceAccount = balanceAccount;
        }
        public Long getAccId() {
            return accId;
        }
        public String getFullAcc() {
            return fullAcc;
        }
        public String getBranchCode() {
            return branchCode;
        }
        public Currency getCurrency() {
            return currency;
        }
        public BalanceAccount getBalanceAccount() {
            return balanceAccount;
        }
    }
}
