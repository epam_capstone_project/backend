package uz.app.banking.mockobjects;

public class TestUsers {
    public static final TestUser admin = new TestUser("admin", "asd123+");
    public static final TestUser emp = new TestUser("employee", "asd123+");
    public static final TestUser client = new TestUser("client", "asd123+");
    public static final TestUser newUser = new TestUser("newEmp", "asd123+");

    public static class TestUser {
        public String login;
        public String password;

        public TestUser(String login, String password) {
            this.login = login;
            this.password = password;
        }
    }
}
