package uz.app.banking.security.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import uz.app.banking.TestConfig;
import uz.app.banking.mockobjects.TestUsers;
import uz.app.banking.security.dto.LoginRequest;
import uz.app.banking.security.dto.RegisterRequest;
import uz.app.banking.security.dto.RegisterResponse;

// @RunWith(SpringJUnit4ClassRunner.class)
// @EnableAutoConfiguration
// @ContextConfiguration(classes = App.class)

// @ExtendWith(SpringExtension.class)
// @SpringBootTest(webEnvironment = WebEnvironment.NONE,
//     classes = App.class)
// @AutoConfigureMockMvc

// @Configuration
// @RunWith(SpringRunner.class)
// @SpringBootTest(webEnvironment = WebEnvironment.NONE)
// @PropertySource("test.properties")
@SpringBootTest
public class AuthControllerTest {

    AuthController authController;
    ApplicationContext context;

    Class<?> loginResponseClass;
    Field errCodeField;
    Field errMsgField;

    @Before
    public void init() throws Exception {
        context = TestConfig.getContext();
        authController = context.getBean(AuthController.class);
        loginResponseClass = Class.forName("uz.app.banking.security.dto.LoginResponse");
        errCodeField = loginResponseClass.getDeclaredField("errCode");
        errCodeField.setAccessible(true);
        errMsgField = loginResponseClass.getDeclaredField("errMsg");
    }

    @Test
    public void testLoginSuccess() throws Exception {
        ResponseEntity<?> responseEntity = login(this.context, TestUsers.admin.login, TestUsers.admin.password);
        assertEquals(responseEntity.getStatusCodeValue(), 200);
        assertTrue(responseEntity.getBody().getClass().equals(loginResponseClass));
        Object getErrCodeResult = errCodeField.get(responseEntity.getBody());
        assertTrue(getErrCodeResult instanceof String);
        assertEquals(String.valueOf(getErrCodeResult), "0");
        assertEquals(((UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername(), "admin");
    }

    @Test
    public void testLoginFail() throws Exception {
        String randomPassword = UUID.randomUUID().toString();
        while(randomPassword.equals(TestUsers.admin.login))
            randomPassword = UUID.randomUUID().toString();
        ResponseEntity<?> responseEntity = login(this.context, TestUsers.admin.login, randomPassword);
        assertEquals(responseEntity.getStatusCodeValue(), 401);
        Object getErrCodeResult = errCodeField.get(responseEntity.getBody());
        assertTrue(getErrCodeResult instanceof String);
        assertEquals(String.valueOf(getErrCodeResult), "1");
        assertNull(SecurityContextHolder.getContext().getAuthentication());
    }

    public static ResponseEntity<?> login(ApplicationContext context, String login, String password) {
        AuthController authController = context.getBean(AuthController.class);
        SecurityContextHolder.clearContext();
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setLogin(login);
        loginRequest.setPassword(password);
        return authController.login(loginRequest);
    }
}
