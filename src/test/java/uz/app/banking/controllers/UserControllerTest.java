package uz.app.banking.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import uz.app.banking.TestConfig;
import uz.app.banking.dto.SavingResponseDTO;
import uz.app.banking.dto.UserDTO;
import uz.app.banking.dto.UserInfoDTO;
import uz.app.banking.entities.User;
import uz.app.banking.mockobjects.TestUsers;
import uz.app.banking.references.UserType;
import uz.app.banking.repositories.UserRepository;
import uz.app.banking.security.controllers.AuthControllerTest;

@SpringBootTest
public class UserControllerTest {

    UserController userController;

    UserRepository userRepo;
    
    ApplicationContext context;


    @Before
    public void init() {
        context = TestConfig.getContext();
        userController = context.getBean(UserController.class);
        userRepo = context.getBean(UserRepository.class);
    }

    @Test
    public void testAccessForAdmin() {
        AuthControllerTest.login(context, TestUsers.admin.login, TestUsers.admin.password);
        testGetAll();
        testGetById();
        testGetByLogin();
        testCallSave();
    }

    @Test
    public void testAccessForEmployee() {
        AuthControllerTest.login(context, TestUsers.emp.login, TestUsers.emp.password);
        Exception ex = assertThrows(RuntimeException.class, () -> {
            testGetAll();
            testGetById();
            testGetByLogin();
            testCallSave();
        });
        assertNotNull(ex);
    }

    @Test
    public void testAccessForClient() {
        AuthControllerTest.login(context, TestUsers.client.login, TestUsers.client.password);
        Exception ex = assertThrows(RuntimeException.class, () -> {
            testGetAll();
            testGetById();
            testGetByLogin();
            testCallSave();
        });
        assertNotNull(ex);
    }

    public void testGetAll() {
        this.userController.getAll(Pageable.unpaged());
    }

    public void testGetById() {
        this.userController.getById(1l);
    }

    public void testGetByLogin() {
        this.userController.getByLogin(((UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
    }

    public void testCallSave() {
        AuthControllerTest.login(context, TestUsers.admin.login, TestUsers.admin.password);
        UserInfoDTO userInfo = new UserInfoDTO();
        userInfo.setLogin(TestUsers.newUser.login);
        userInfo.setUserType(UserType.EMPLOYEE);
        UserDTO user = new UserDTO(userInfo, TestUsers.newUser.password);
    }

    @Test
    public void testSavePositive() {
        AuthControllerTest.login(context, TestUsers.admin.login, TestUsers.admin.password);
        UserInfoDTO userInfo = new UserInfoDTO();
        userInfo.setLogin(TestUsers.newUser.login);
        userInfo.setUserType(UserType.EMPLOYEE);
        UserDTO user = new UserDTO(userInfo, TestUsers.newUser.password);
        ResponseEntity<SavingResponseDTO<UserInfoDTO>> respEntity = this.userController.save(user);
        // ******** CHECK *********
        assertEquals(respEntity.getStatusCodeValue(), 200);
        assertEquals(respEntity.getBody().getErrCode(), "0");
        assertEquals(respEntity.getBody().getData().getLogin(), user.getLogin());
        User savedUser = userRepo.findByLogin(user.getLogin()).get();
        assertEquals(userInfo.getLogin(), savedUser.getLogin());
    }
}
