package uz.app.banking.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import uz.app.banking.TestConfig;
import uz.app.banking.dto.EmployeeDTO;
import uz.app.banking.mockobjects.TestUsers;
import uz.app.banking.security.controllers.AuthControllerTest;
import uz.app.banking.services.UserService;

public class EmployeeControllerTest {
    ApplicationContext context;
    EmployeeController employeeController;
    UserService userService;

    @Before
    public void init() {
        context = TestConfig.getContext();
        employeeController = context.getBean(EmployeeController.class);
        userService = context.getBean(UserService.class);
    }

    @Test
    public void testAccessAsAdmin() {
        AuthControllerTest.login(context, TestUsers.admin.login, TestUsers.admin.password);
        testGetAll();
        testGetById();
        assertThrows(RuntimeException.class, this::testGetMe);
    }

    @Test
    public void testAccessAsEmp() {
        AuthControllerTest.login(context, TestUsers.emp.login, TestUsers.emp.password);
        assertThrows(RuntimeException.class, this::testGetAll);
        assertThrows(RuntimeException.class, this::testGetById);
        // Access granted for employee
        testGetMe();
    }

    @Test
    public void testAccessAsClient() {
        AuthControllerTest.login(context, TestUsers.client.login, TestUsers.client.password);
        assertThrows(RuntimeException.class, this::testGetAll);
        assertThrows(RuntimeException.class, this::testGetById);
        assertThrows(RuntimeException.class, this::testGetMe);
    }

    public void testGetAll() {
        employeeController.getAll(Pageable.unpaged());
    }

    public void testGetById() {
        employeeController.getById(1l);
    }

    public void testGetMe() {
        ResponseEntity<EmployeeDTO> resp = employeeController.getMe();
        assertEquals(resp.getStatusCodeValue(), 200);
        EmployeeDTO emp = resp.getBody();
        UserDetails loggedUsed = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        assertEquals(userService.getUserByLogin(loggedUsed.getUsername()).get().getId(), emp.getUserId());
    }
}
