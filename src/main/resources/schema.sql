drop table if exists public.Rules;
drop table if exists public.Transactions;
drop table if exists public.Accounts;
drop table if exists public.Employees;
drop table if exists public.Clients;
drop table if exists public.Users;
drop table if exists public.Branches;
drop table if exists public.Ref_User_Types;
drop table if exists public.Ref_Transaction_States;
drop table if exists public.Ref_Balance_Accounts;
drop table if exists public.Ref_Currencies;
drop table if exists public.Ref_Countries;
drop table if exists public.Ref_Doc_Types;


CREATE TABLE public.Ref_Currencies
(
    code character varying(3) NOT NULL,
    name character varying(100) NOT NULL,
    short_name character varying(4) NOT NULL,
    CONSTRAINT Ref_Currencies_PK PRIMARY KEY (code)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.Ref_Currencies
    OWNER to postgres;



CREATE TABLE public.Ref_Countries
(
    code character varying(3) NOT NULL,
    name character varying(100) NOT NULL,
    short_name character varying(4) NOT NULL,
    CONSTRAINT Ref_Countries_PK PRIMARY KEY (code)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.Ref_Countries
    OWNER to postgres;



CREATE TABLE public.Ref_Doc_Types
(
    code numeric(2) NOT NULL,
    name character varying(100) NOT NULL,
    CONSTRAINT Ref_Doc_Type_U1 UNIQUE (code)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.Ref_Doc_Types
    OWNER to postgres;




CREATE TABLE public.Ref_Balance_Accounts
(
    balance_acc character varying(3) NOT NULL,
    purpose character varying(400),
    acc_type character varying(1) NOT NULL,
    PRIMARY KEY (balance_acc)
)
WITH (
    OIDS = FALSE
);
ALTER TABLE public.Ref_Balance_Accounts
    OWNER to postgres;




CREATE TABLE public.Ref_Transaction_States
(
    code smallint NOT NULL,
    name character varying(200) NOT NULL,
    CONSTRAINT Ref_Transaction_States_U1 UNIQUE (code)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.Ref_Transaction_States
    OWNER to postgres;



CREATE TABLE public.Ref_User_Types
(
    code smallint NOT NULL,
    name character varying(200) NOT NULL,
    CONSTRAINT Ref_User_Types_U1 UNIQUE (code)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.Ref_User_Types
    OWNER to postgres;



CREATE TABLE public.Branches
(
    code character varying(3) NOT NULL,
    name character varying(200) NOT NULL,
    region character varying(200) NOT NULL,
    CONSTRAINT Branches_PK PRIMARY KEY (code)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.Branches
    OWNER to postgres;




CREATE TABLE public.Users
(
    user_id bigserial NOT NULL,
    login character varying(15) NOT NULL,
    password_hash text NOT NULL,
    user_type smallint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by bigint,
    CONSTRAINT Users_PK PRIMARY KEY (user_id),
    CONSTRAINT Users_U1 UNIQUE (login)
) WITH (
    OIDS = FALSE
);

ALTER TABLE public.Users
    OWNER to postgres;




CREATE TABLE public.Clients
(
    client_id bigserial NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    middle_name character varying(30) NOT NULL,
    residency character varying(3) NOT NULL,
    doc_type character varying(2) NOT NULL,
    doc_seria character varying(2) NOT NULL,
    doc_number character varying(20) NOT NULL,
    branch_code character varying(4) NOT NULL,
    user_id bigint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by bigint,
    CONSTRAINT Clients_PK PRIMARY KEY (client_id),
    CONSTRAINT Clients_FK1 FOREIGN KEY (user_id)
        REFERENCES public.Users (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT Clients_FK2 FOREIGN KEY (created_by)
        REFERENCES public.Users (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
) WITH (
    OIDS = FALSE
);
ALTER TABLE public.Clients
    OWNER to postgres;
ALTER TABLE public.Clients
    ADD CONSTRAINT Clients_U1 UNIQUE (doc_type, doc_seria, doc_number, branch_code);




CREATE TABLE public.Employees
(
    emp_id bigserial NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    middle_name character varying(30) NOT NULL,
    branch_code character varying(3) NOT NULL,
    department character varying(100),
    created_at timestamp without time zone NOT NULL,
    created_by bigint,
    user_id bigint NOT NULL,
    CONSTRAINT Employees_PK PRIMARY KEY (emp_id),
    CONSTRAINT Employees_FK1 FOREIGN KEY (created_by)
        REFERENCES public.Users (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT Employees_FK2 FOREIGN KEY (user_id)
        REFERENCES public.Users (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
WITH (
    OIDS = FALSE
);
ALTER TABLE public.Employees
    OWNER to postgres;




CREATE TABLE public.Accounts
(
    acc_id bigserial NOT NULL,
    balance_acc character varying(3) NOT NULL,
    full_acc character varying(18) NOT NULL,
    currency character varying(3) NOT NULL,
    available_balance bigint NOT NULL,
    actual_balance bigint NOT NULL,
    negative_balance_enabled boolean NOT NULL,
    client_id bigint NOT NULL,
    branch_code character varying(3) NOT NULL,
    frozen boolean,
    turnover_all_debit bigint NOT NULL,
    turnover_all_credit bigint NOT NULL,
    created_at date NOT NULL,
    created_by bigint NOT NULL,
    CONSTRAINT Accounts_PK1 PRIMARY KEY (acc_id),
    CONSTRAINT Accounts_U1 UNIQUE (full_acc),
    CONSTRAINT Accounts_FK1 FOREIGN KEY (balance_acc)
        REFERENCES public.Ref_Balance_Accounts (balance_acc) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT Accounts_FK2 FOREIGN KEY (currency)
        REFERENCES public.Ref_Currencies (code) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT Accounts_FK3 FOREIGN KEY (client_id)
        REFERENCES public.Clients (client_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT Accounts_FK4 FOREIGN KEY (branch_code)
        REFERENCES public.Branches (code) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT Accounts_FK5 FOREIGN KEY (created_by)
        REFERENCES public.Users (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
WITH (
    OIDS = FALSE
);
ALTER TABLE public.Accounts
    OWNER to postgres;




CREATE TABLE public.Transactions
(
    tr_id bigserial NOT NULL,
    credit_acc bigint NOT NULL,
    debit_acc bigint NOT NULL,
    sum bigint NOT NULL,
    currency character varying(3) NOT NULL,
    purpose character varying(200),
    branch_code character varying(3) NOT NULL,
    state smallint NOT NULL,
    created_at timestamp without time zone NOT NULL,
    created_by bigint NOT NULL,
    CONSTRAINT Transactions_PK1 PRIMARY KEY (tr_id),
    CONSTRAINT Transactions_FK1 FOREIGN KEY (currency)
        REFERENCES public.Ref_Currencies (code) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT Transactions_FK2 FOREIGN KEY (branch_code)
        REFERENCES public.Branches (code) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT Transactions_FK3 FOREIGN KEY (created_by)
        REFERENCES public.Users (user_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID,
    CONSTRAINT Transactions_FK4 FOREIGN KEY (state)
        REFERENCES public.Ref_Transaction_States (code) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
        NOT VALID
)
WITH (
    OIDS = FALSE
);
ALTER TABLE public.Transactions
    OWNER to postgres;




CREATE TABLE public.Rules
(
    rule_id serial NOT NULL,
    creditor_balance_acc character varying(3) NOT NULL,
    debitor_balance_acc character varying(3) NOT NULL,
    currency character varying(3) NOT NULL,
    sum_upper_bound numeric(30, 2),
    lead_state smallint,
    created_at timestamp without time zone NOT NULL,
    created_by bigint NOT NULL,
    CONSTRAINT Rules_PK1 PRIMARY KEY (rule_id)
)
WITH (
    OIDS = FALSE
);

ALTER TABLE public.Rules
    OWNER to postgres;