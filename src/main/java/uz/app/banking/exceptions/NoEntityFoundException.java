package uz.app.banking.exceptions;

public class NoEntityFoundException extends RuntimeException {
    private Object id;
    private Class<?> entityClass;

    public NoEntityFoundException(Object id, Class<?> entityClass) {
        this.id = id;
        this.entityClass = entityClass;
    }

    @Override
    public String getMessage() {
        return String.format("There's no entity <%s> with id = %s", entityClass.getSimpleName(), this.id);
    }
}
