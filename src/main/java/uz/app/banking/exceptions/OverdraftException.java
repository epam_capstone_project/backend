package uz.app.banking.exceptions;

public class OverdraftException extends RuntimeException {
    private String creditFullAcc;
    private String debitFullAcc;
    private Long sum;

    public OverdraftException(String creditFullAcc, String debitFullAcc, Long sum) {
        this.creditFullAcc = creditFullAcc;
        this.debitFullAcc = debitFullAcc;
        this.sum = sum;
    }

    public String getCreditFullAcc() {
        return creditFullAcc;
    }

    public String getDebitFullAcc() {
        return debitFullAcc;
    }

    public Long getSum() {
        return sum;
    }

    @Override
    public String getMessage() {
        return String.format("Overdraft occurred. Cr: %s; Deb: %s; Sum: %d", this.creditFullAcc, this.debitFullAcc, this.sum);
    }
    
}
