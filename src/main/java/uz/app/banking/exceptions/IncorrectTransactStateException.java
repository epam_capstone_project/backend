package uz.app.banking.exceptions;

public class IncorrectTransactStateException extends RuntimeException {
    public IncorrectTransactStateException(String message) {
        super(message);
    }
}
