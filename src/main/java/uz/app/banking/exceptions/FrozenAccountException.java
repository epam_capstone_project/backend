package uz.app.banking.exceptions;

public class FrozenAccountException extends RuntimeException {
    private String frozenFullAcc;
    public FrozenAccountException(String fullAcc) {
        this.frozenFullAcc = fullAcc;
    }
    
    public String getFrozenFullAcc() {
        return frozenFullAcc;
    }

    @Override
    public String getMessage() {
        return String.format("Operation on frozen account. Acc: %s", this.frozenFullAcc);
    }
}
