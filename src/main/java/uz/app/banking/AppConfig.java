package uz.app.banking;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;

@Configuration
public class AppConfig {
    /* 
     This bean is necessary because Spring Security's MvcRequestMatcher expects a HandlerMappingIntrospector bean 
     registered by our Spring MVC config that is used to perform the url matching. 
     If removed, it will show its issues at testing stage.
    */
    @Bean(name = "mvcHandlerMappingIntrospector")
    public HandlerMappingIntrospector mvcHandlerMappingIntrospector() {
        return new HandlerMappingIntrospector();
    }
}
