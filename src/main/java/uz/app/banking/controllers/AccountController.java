package uz.app.banking.controllers;

import org.springframework.web.bind.annotation.RestController;

import uz.app.banking.dto.AccountCreateReqDTO;
import uz.app.banking.dto.AccountCreateRespDTO;
import uz.app.banking.dto.AccountDTO;
import uz.app.banking.dto.StandardResponseDTO;
import uz.app.banking.services.AccountService;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;




@RestController()
@RequestMapping(path = "/account", method=RequestMethod.GET)
public class AccountController {
    @Autowired
    private AccountService accountService;


    @GetMapping("/get")
    public Page<AccountDTO> getAll(Pageable page) {
        return accountService.getAll(page);
    }

    @GetMapping("/getBy/client/{client_Id}")
    public Page<AccountDTO> getByClientId(@PathVariable("client_Id") Long clientId, Pageable page) {
        return accountService.getByClient(clientId, page);
    }
    
    @GetMapping("/getBy/balanceAcc")
    public Page<AccountDTO> getByBalanceAccLike(@RequestParam("like") String balanceAccLike, Pageable page) {
        return accountService.getByBalanceAccLike(balanceAccLike, page);
    }

    @GetMapping("/getBy/currency")
    public Page<AccountDTO> getByCurrencyLike(@RequestParam("like") String curencyLike, Pageable page) {
        return accountService.getByCurrencyLike(curencyLike, page);
    }

    @GetMapping("/getBy/id/{acc_id}")
    public ResponseEntity<AccountDTO> getById(@PathVariable("acc_id") Long accId) {
        return ResponseEntity.of(accountService.getById(accId));
    }

    @Transactional
    @PostMapping("/create")
    public ResponseEntity<AccountCreateRespDTO> createAccount(@RequestBody AccountCreateReqDTO reqDTO) {
        return ResponseEntity.ok(accountService.createAcc(reqDTO));
    }
    
    @PutMapping("/freeze/{accId}")
    public ResponseEntity<StandardResponseDTO> freezeAcc(@PathVariable("accId") Long accId) {
        accountService.setFrozenAcc(accId, true);
        return ResponseEntity.ok(new StandardResponseDTO("0", "Success"));
    }

    @PutMapping("/unfreeze/{accId}")
    public ResponseEntity<StandardResponseDTO> unfreezeAcc(@PathVariable("accId") Long accId) {
        accountService.setFrozenAcc(accId, false);
        return ResponseEntity.ok(new StandardResponseDTO("0", "Success"));
    }
}
