package uz.app.banking.controllers;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import uz.app.banking.dto.SavingResponseDTO;
import uz.app.banking.dto.UserDTO;
import uz.app.banking.dto.UserInfoDTO;
import uz.app.banking.services.UserService;

@RestController
@RequestMapping(path = "/user", produces = MediaType.APPLICATION_JSON_VALUE)
@Secured("ROLE_ADMIN")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/get/id/{id}")
    public ResponseEntity<UserInfoDTO> getById(@PathVariable(name = "id") Long Id) {
        return ResponseEntity.of(userService.getUserById(Id));
    }

    @GetMapping("/get/login/{login}")
    public ResponseEntity<UserInfoDTO> getByLogin(@PathVariable(name = "login") String login) {
        return ResponseEntity.of(userService.getUserByLogin(login));
    }

    @Transactional
    @PostMapping("/save")
    public ResponseEntity<SavingResponseDTO<UserInfoDTO>> save(@RequestBody UserDTO userDTO) {
        try {
            UserInfoDTO userInfoDTO = userService.save(userDTO);
            return ResponseEntity.ok(new SavingResponseDTO<UserInfoDTO>(userInfoDTO, "0", "Success"));
        } catch (Exception e) {
            return ResponseEntity.status(400).body(new SavingResponseDTO<UserInfoDTO>(null, "1", "Not saved. Reason: " + e.getMessage()));
        }
    }

    @GetMapping("/get")
    public Page<UserInfoDTO> getAll(Pageable page) {
        return userService.getAll(page);
    }
}
