package uz.app.banking.controllers;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import uz.app.banking.dto.SavingResponseDTO;
import uz.app.banking.dto.StandardResponseDTO;
import uz.app.banking.dto.TransactInfoDTO;
import uz.app.banking.exceptions.NoEntityFoundException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PutMapping;
import uz.app.banking.services.TransactService;


@Secured("ROLE_EMPLOYEE")
@RestController
@RequestMapping(
    path = "/transact", 
    //consumes = MediaType.APPLICATION_JSON_VALUE, 
    produces = MediaType.APPLICATION_JSON_VALUE)
public class EmployeeTransactController {

    @Autowired
    private TransactService transactService;

    @GetMapping("/get")
    public Page<TransactInfoDTO> getFiltered(
        @RequestParam(value = "creditorAcc", required = false) String creditorAcc, 
        @RequestParam(value = "debitorAcc", required = false) String debitorAcc, 
        @RequestParam(value = "currency", required = false) String currencyCode, 
        @RequestParam(value = "branch", required = false) String branchCode,
        @RequestParam(value = "clientId", required = false) Long clientId,
        Pageable page) {
        return transactService.get(creditorAcc, debitorAcc, currencyCode, branchCode, clientId, page);
    }
    
    @PostMapping("/create")
    public SavingResponseDTO<TransactInfoDTO> create(@RequestBody TransactInfoDTO transact) {
        try {
            return new SavingResponseDTO<TransactInfoDTO>(transactService.create(transact), "0", "Success");
        } catch (NoEntityFoundException e) {
            return new SavingResponseDTO<TransactInfoDTO>(null, "404", e.getMessage());
        }
    }

    @PutMapping("edit")
    public SavingResponseDTO<TransactInfoDTO> editTransactDetails(@RequestBody TransactInfoDTO transact) {
        try {
            return new SavingResponseDTO<TransactInfoDTO>(transactService.edit(transact), "0", "Success");
        } catch (NoEntityFoundException e) {
            return new SavingResponseDTO<TransactInfoDTO>(null, "404", e.getMessage());
        }
    }
    
    @PutMapping("approve/{id}")
    public StandardResponseDTO approveTransact(@PathVariable Long id) {
        try {
            transactService.approve(id);
            return new StandardResponseDTO("0", "Success");
        } catch (NoEntityFoundException e) {
            return new StandardResponseDTO("404", "Transaction not found");
        }
    }

    @PutMapping("cancel/{id}")
    public StandardResponseDTO cancelTransact(@PathVariable Long id) {
        try {
            transactService.cancel(id);
            return new StandardResponseDTO("0", "Success");
        } catch (NoEntityFoundException e) {
            return new StandardResponseDTO("404", "Transaction not found");
        }
    }
    

}
