package uz.app.banking.controllers;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import uz.app.banking.dto.SavingResponseDTO;
import uz.app.banking.dto.StandardResponseDTO;
import uz.app.banking.dto.TransactInfoDTO;
import uz.app.banking.exceptions.NoEntityFoundException;
import uz.app.banking.services.TransactService;
import uz.app.banking.services.UserService;

@Secured("ROLE_CLIENT")
@RestController
@RequestMapping(
    path = "/transact/client", 
    consumes = MediaType.APPLICATION_JSON_VALUE, 
    produces = MediaType.APPLICATION_JSON_VALUE)
public class ClientTransactController {
    @Autowired
    private TransactService transactService;

    @Autowired
    private UserService userService;
    
    @GetMapping("/get")
    public Page<TransactInfoDTO> getMyTransacts(
        @RequestParam("creditorAcc") String creditorAcc, 
        @RequestParam("debitorAcc") String debitorAcc, 
        @RequestParam("currency") String currencyCode, 
        @RequestParam("branch") String branchCode,
        Pageable page) {
        return transactService.get(creditorAcc, debitorAcc, currencyCode, branchCode, userService.getCurrentUser().get().getId(), page);
    }

    @PostMapping("/create")
    public SavingResponseDTO<TransactInfoDTO> create(@RequestBody TransactInfoDTO transact) {
        try {
            return new SavingResponseDTO<TransactInfoDTO>(transactService.create(transact), "0", "Success");
        } catch (NoEntityFoundException e) {
            return new SavingResponseDTO<TransactInfoDTO>(null, "404", e.getMessage());
        }
    }

    @PutMapping("approve/{id}")
    public StandardResponseDTO approveTransact(@PathVariable Long id) {
        try {
            transactService.approve(id);
            return new StandardResponseDTO("0", "Success");
        } catch (NoEntityFoundException e) {
            return new StandardResponseDTO("404", "Success");
        }
    }
}
