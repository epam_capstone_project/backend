package uz.app.banking.controllers;

import java.util.NoSuchElementException;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import uz.app.banking.dto.ClientDTO;
import uz.app.banking.dto.ClientSaveReqDTO;
import uz.app.banking.dto.ClientSaveRespDTO;
import uz.app.banking.dto.SavingResponseDTO;
import uz.app.banking.dto.StandardResponseDTO;
import uz.app.banking.dto.UserInfoDTO;
import uz.app.banking.repositories.BranchRepository;
import uz.app.banking.services.ClientService;
import uz.app.banking.services.UserService;

@RestController
@RequestMapping(path = "/client", produces = MediaType.APPLICATION_JSON_VALUE)
public class ClientController {
    @Autowired
    ClientService clientService;

    @Autowired
    UserService userService;

    @Autowired
    BranchRepository branchRepo;

    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    @GetMapping("/get/id/{id}")
    public ResponseEntity<ClientDTO> getById(@PathVariable("id") Long clientId) {
        return ResponseEntity.of(clientService.getById(clientId));
    }

    @PreAuthorize("hasRole('CLIENT')")
    @GetMapping("/getMe")
    public ResponseEntity<ClientDTO> getMe() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        UserInfoDTO user = userService.getUserByLogin(userDetails.getUsername()).get();
        return ResponseEntity.of(clientService.getByUserId(user.getId()));
    }

    @PreAuthorize("hasRole('EMPLOYEE') or hasRole('ADMIN')")
    @GetMapping("/get")
    public Page<ClientDTO> getAll(Pageable page) {
        return clientService.getAll(page);
    }

    @PreAuthorize("hasRole('EMPLOYEE')")
    @PostMapping("/create")
    @Transactional
    public ResponseEntity<SavingResponseDTO<ClientSaveRespDTO>> create(@RequestBody ClientSaveReqDTO clientSaveRequest) {
        try {
            ClientSaveRespDTO clientSaveRespDTO = clientService.create(clientSaveRequest);
            return ResponseEntity.ok(new SavingResponseDTO<ClientSaveRespDTO>(clientSaveRespDTO, "0", "Success"));
        } catch(NoSuchElementException ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new SavingResponseDTO<ClientSaveRespDTO>(null, "2", "Trying to update not existing client"));
        } catch(Exception ex) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new SavingResponseDTO<ClientSaveRespDTO>(null, "1", "Not saved. Reason: " + ex.getMessage()));
        }
    }

    @PreAuthorize("hasRole('EMPLOYEE')")
    @PutMapping("/update")
    @Transactional
    public ResponseEntity<StandardResponseDTO> update(@RequestBody ClientSaveReqDTO clientSaveReqDTO) {
        return ResponseEntity.ok(clientService.update(clientSaveReqDTO));
    }
}
