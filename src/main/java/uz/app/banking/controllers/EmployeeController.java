package uz.app.banking.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import uz.app.banking.dto.EmployeeDTO;
import uz.app.banking.dto.UserInfoDTO;
import uz.app.banking.services.EmployeeService;
import uz.app.banking.services.UserService;

@RestController
@RequestMapping(path = "/emp", produces = MediaType.APPLICATION_JSON_VALUE)
public class EmployeeController {
    @Autowired
    private UserService userService;

    @Autowired
    private EmployeeService empService;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/get/id/{id}")
    public ResponseEntity<EmployeeDTO> getById(@PathVariable("id") Long id) {
        return ResponseEntity.of(empService.getById(id));
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/get")
    public Page<EmployeeDTO> getAll(Pageable page) {
        return empService.getAll(page);
    }

    @PreAuthorize("hasRole('EMPLOYEE')")
    @GetMapping("/getMe")
    public ResponseEntity<EmployeeDTO> getMe() {
        UserInfoDTO user = userService.getCurrentUser().get();
        return ResponseEntity.of(empService.getByUserId(user.getId()));
    }
}
