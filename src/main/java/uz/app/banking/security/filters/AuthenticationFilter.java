package uz.app.banking.security.filters;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import uz.app.banking.security.util.JwtUtil;


public class AuthenticationFilter extends OncePerRequestFilter {
    final UserDetailsService userService;
    final AuthenticationManager authenticationManager;
    final JwtUtil jwtUtil;

    public AuthenticationFilter(UserDetailsService userService, AuthenticationManager authManager, JwtUtil jwtUtil) {
        this.userService = userService;
        this.authenticationManager = authManager;
        this.jwtUtil = jwtUtil;
    }

    private static String getToken(HttpServletRequest req) {
        String authenticationHeader = req.getHeader("Authorization");
        if(authenticationHeader != null && authenticationHeader.startsWith("Bearer"))
            return authenticationHeader.substring("Bearer ".length());
        else
            return null;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
            throws ServletException, IOException {
        String token = getToken(req);

        if(jwtUtil.validateToken(token)) {
            String login = jwtUtil.exctractLogin(token);
            UserDetails user = userService.loadUserByUsername(login);
            Authentication auth = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(auth);
        }
        chain.doFilter(req, res);
    }
}
