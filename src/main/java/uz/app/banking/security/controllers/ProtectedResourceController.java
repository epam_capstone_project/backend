package uz.app.banking.security.controllers;

import java.util.stream.Collectors;

import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProtectedResourceController {
    
    @GetMapping(value = "/protected", produces = MediaType.APPLICATION_JSON_VALUE)
    @Secured({"ROLE_ADMIN", "ROLE_EMPLOYEE"})
    public String protectedResource() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails user = (UserDetails)auth.getPrincipal();
        String roles = String.valueOf(user.getAuthorities().stream().map(x -> x.getAuthority()).collect(Collectors.toList()));
        return "{\"message\": \"This is a protected resource.\", \"roles\": \""+roles+"\"}";
    }
}
