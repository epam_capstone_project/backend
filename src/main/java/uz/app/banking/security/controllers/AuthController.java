package uz.app.banking.security.controllers;

import java.util.Optional;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import uz.app.banking.security.dto.LoginRequest;
import uz.app.banking.security.dto.LoginResponse;
import uz.app.banking.security.dto.RegisterRequest;
import uz.app.banking.security.dto.RegisterResponse;
import uz.app.banking.security.util.JwtUtil;

@RestController
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class AuthController {
    static final Logger log = LoggerFactory.getLogger(AuthController.class);

    @Autowired
    UserDetailsService userService;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    JwtUtil jwtUtil;

    @PostMapping(value = "/login")
    public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest request) {

        Authentication auth = null;
        try {
            auth = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                    request.getLogin(),
                    request.getPassword()));
        } catch (Exception ex) {
            log.debug("Error on AuthController", ex);
            return ResponseEntity.status(401).body(new LoginResponse("1", "Login or password is incorrect", ""));
        }

        SecurityContextHolder.getContext().setAuthentication(auth);
        String token = jwtUtil.generateToken(request.getLogin());
        return ResponseEntity.of(Optional.of(new LoginResponse("0", "success", token)));
    }

    @GetMapping(value = "/userInfo")
    public String getUserInfo() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        /*return ResponseEntity.of(
            Optional.of(
                new UserInfoResponse(
                    String.valueOf(auth.getPrincipal()), 
                    String.valueOf(auth.getCredentials()), 
                    new String[] {auth.getAuthorities().stream().map(x->x.getAuthority()).reduce(
                        (x,y)-> String.valueOf(x)+ " " + String.valueOf(y)
                    ).get()}
                )
            )
        );*/
        UserDetails user = (UserDetails)auth.getPrincipal();
        return user.getUsername() + " " + user.getPassword() + " " + String.valueOf(user.getAuthorities().stream().map(x ->x.getAuthority()).collect(Collectors.toList()));
        //return auth.getPrincipal() + " " + String.valueOf(auth.getAuthorities().stream().map(x -> x.getAuthority()).collect(Collectors.toList()));
    }

    @PostMapping("/register")
    public ResponseEntity<RegisterResponse> register(@RequestBody RegisterRequest req) {
        if(userService instanceof InMemoryUserDetailsManager) {
            InMemoryUserDetailsManager userDetailsManager = (InMemoryUserDetailsManager)userService;
            UserDetails newUser = User.builder()
                .username(req.getLogin())
                .password(req.getPassword())
                .passwordEncoder(p -> passwordEncoder.encode(p))
                .authorities(new SimpleGrantedAuthority("ROLE_USER"))
                .build();

            userDetailsManager.createUser(newUser);
            return ResponseEntity.ok(new RegisterResponse("0", "Successfully created"));
        } else {
            return ResponseEntity.ok(new RegisterResponse("1", "User service not set"));
        }
    }
}
