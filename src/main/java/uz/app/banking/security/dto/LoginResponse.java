package uz.app.banking.security.dto;

public class LoginResponse {
    private String token;
    private String errCode;
    private String errMsg;

    public LoginResponse(String errCode, String errMsg, String token) {
        this.token = token;
        this.errCode = errCode;
        this.errMsg = errMsg;
    }
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public String getErrCode() {
        return errCode;
    }
    public void setErrCode(String errCode) {
        this.errCode = errCode;
    }
    public String getErrMsg() {
        return errMsg;
    }
    public void setErrMsg(String errMsg) {
        this.errMsg = errMsg;
    }

    
}
