package uz.app.banking.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import uz.app.banking.entities.User;
import uz.app.banking.repositories.UserRepository;

public class UserDetailsService implements org.springframework.security.core.userdetails.UserDetailsService {
    @Autowired
    UserRepository userRepo;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = userRepo.findByLogin(userName).orElseThrow(() -> new UsernameNotFoundException("User not found: " + userName));
        return org.springframework.security.core.userdetails.User.builder().username(user.getLogin()).password(user.getPasswordHash()).roles(user.getUserType().toString()).build();
    }
}
