package uz.app.banking.references;

import java.util.stream.*;

public enum TransactState {
    WAITING_APPROVAL(0, "Waiting for approval of client"),
    APPROVED(1, "Approved by client"),
    NOT_APPROVED(2, "Not approved by client"),
    CANCELLED(3, "Canceled by bank employee")
    ;
    private int code;
    private String name;
    private TransactState(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public static TransactState getByCode(int code) {
        return Stream.of(TransactState.values()).filter(x -> x.code == code).findFirst().get();
    }
}
