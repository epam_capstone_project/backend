package uz.app.banking.references;

import java.util.stream.Stream;

public enum BalanceAccount {
    CLIENT_PLAST_CARD("555", "Client plastic card"),
    CLIENT_VIRTUAL_WALLET("510", "Virtual wallet account for client"),
    CLIENT_DEPOSIT("620", "Client deposit"),
    CLIENT_CREDIT("630", "Client credit"),
    TRANSIT_INCOME("200", "Transit income"),
    TRANSIT_OUTCOME("210", "Transit outcome"),
    BANK_PROFIT("001", "Bank profit"),
    BANK_LOSS("002", "Bank loss"),
    INTERBANK_IN("101", "Interbank IN"),
    INTERBANK_OUT("102", "Interbank OUT")
    ;


    private String code;
    public String getCode() {
        return code;
    }

    private String purpose;
    public String getPurpose() {
        return purpose;
    }

    private String accType;
    public String getAccType() {
        return accType;
    }

    BalanceAccount(String code, String purpose) {
        this.code = code;
        this.purpose = purpose;
    }

    public static BalanceAccount getByCode(String code) {
        return Stream.of(BalanceAccount.values()).filter(x -> x.code.equals(code)).findFirst().orElseThrow();
    }
}
