package uz.app.banking.references;

import java.util.stream.Stream;

public enum Currency {
    EURO("978", "Euro", 2),
    USD("840", "US dollars", 2),
    RUB("643", "Russian rouble", 2),
    GBP("826", "GB Pound sterling", 2),
    KZT("398", "Kazakh tenge", 2),
    UZS("860", "Uzbek sum", 2)
    ;

    private String code;
    public String getCode() {
        return code;
    }

    private String name;
    public String getName() {
        return name;
    }

    private int scale;
    public int getScale() {
        return scale;
    }

    private Currency(String code, String name, int scale) {
        this.code = code;
        this.name = name;
        this.scale = scale;
    }

    public static Currency getByCode(String code) {
        return Stream.of(Currency.values()).filter(x -> x.getCode().equals(code)).findFirst().get();
    }
}
