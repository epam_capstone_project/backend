package uz.app.banking.references;

import java.util.stream.Stream;

public enum Country {
    FRA("250", "France"),
    KAZ("398", "Kazakhstan"),
    RUS("643", "Russia"),
    GBR("826", "Great Britain"),
    USA("840", "United states of America"),
    UZB("860", "Uzbekistan");


    private String code;
    private String name;

    private Country(String code, String name) {
        this.code = code;
        this.name = name;
    }
    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public static Country getByCode(String code) {
        return Stream.of(Country.values()).filter(x -> x.getCode().equals(code)).findFirst().get();
    }
}
