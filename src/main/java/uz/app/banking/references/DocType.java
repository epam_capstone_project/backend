package uz.app.banking.references;

import java.util.stream.Stream;

public enum DocType {
    PASSPORT("1", "Passport"),
    ID_CARD("2", "ID-card"),
    DRIVER_LIC("3", "Driver's license");

    private String code;
    private String name;

    private DocType(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public static DocType getByCode(String code) {
        return Stream.of(DocType.values()).filter(x -> x.getCode().equals(code)).findFirst().get();
    }
}
