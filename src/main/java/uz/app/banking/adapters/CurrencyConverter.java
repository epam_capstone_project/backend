package uz.app.banking.adapters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import uz.app.banking.references.Currency;

@Converter(autoApply = true)
public class CurrencyConverter implements AttributeConverter<Currency, String>{
    @Override
    public String convertToDatabaseColumn(Currency value) {
        if(value == null) return null;
        return value.getCode();
    }

    @Override
    public Currency convertToEntityAttribute(String value) {
        if(value == null) return null;
        return Currency.getByCode(value);
    }
}
