package uz.app.banking.adapters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import uz.app.banking.references.TransactState;

@Converter(autoApply = true)
public class TransactionStateConverter implements AttributeConverter<TransactState, Short> {
    @Override
    public Short convertToDatabaseColumn(TransactState value) {
        return (short)value.getCode();
    }

    @Override
    public TransactState convertToEntityAttribute(Short value) {
        return TransactState.getByCode(Integer.valueOf(value));
    }
}
