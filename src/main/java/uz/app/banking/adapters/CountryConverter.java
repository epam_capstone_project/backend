package uz.app.banking.adapters;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import uz.app.banking.references.Country;

@Converter(autoApply = true)
public class CountryConverter implements AttributeConverter<Country, String> {

    @Override
    public String convertToDatabaseColumn(Country value) {
        return value.getCode();
    }

    @Override
    public Country convertToEntityAttribute(String value) {
        return Country.getByCode(value);
    }
    
}
