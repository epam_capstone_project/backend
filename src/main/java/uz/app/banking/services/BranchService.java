package uz.app.banking.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import uz.app.banking.dto.BranchDTO;
import uz.app.banking.repositories.BranchRepository;

@Service
public class BranchService {
    @Autowired
    BranchRepository  branchRepo;

    public Optional<BranchDTO> getByCode(String branchCode) {
        return branchRepo.findById(branchCode).map(x -> BranchDTO.toDTO(x));
    }
}
