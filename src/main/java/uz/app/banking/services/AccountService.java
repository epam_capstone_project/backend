package uz.app.banking.services;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import uz.app.banking.dto.AccountCreateReqDTO;
import uz.app.banking.dto.AccountCreateRespDTO;
import uz.app.banking.dto.AccountDTO;
import uz.app.banking.dto.ClientDTO;
import uz.app.banking.entities.Account;
import uz.app.banking.entities.Branch;
import uz.app.banking.entities.Client;
import uz.app.banking.entities.User;
import uz.app.banking.references.BalanceAccount;
import uz.app.banking.references.Currency;
import uz.app.banking.repositories.AccountRepository;
import uz.app.banking.repositories.BranchRepository;
import uz.app.banking.repositories.ClientRepository;
import uz.app.banking.repositories.UserRepository;

@Service
public class AccountService {
    @Autowired
    private AccountRepository accountRepo;
    @Autowired
    private ClientRepository clientRepo;
    @Autowired
    private BranchRepository branchRepo;
    @Autowired
    private UserRepository userRepo;


    public Page<AccountDTO> getAll(Pageable page) {
        return accountRepo.findAll(page).map(x -> AccountDTO.toDTO(x));
    }

    public Page<AccountDTO> getByCurrencyLike(String like, Pageable page) {
        if(like == null || like.isEmpty())
            return Page.empty();
        List<Currency> listOfInterests = Stream.of(Currency.values())
            .filter(x -> x.getCode().indexOf(like) >= 0)
            .collect(Collectors.toList());
        return accountRepo.findByCurrencyIn(listOfInterests, page).map(x -> AccountDTO.toDTO(x));
    }

    public Page<AccountDTO> getByBalanceAccLike(String like, Pageable page) {
        if(like == null || like.isEmpty())
            return Page.empty();
        List<BalanceAccount> listOfInterests = Stream.of(BalanceAccount.values())
            .filter(x -> x.getCode().indexOf(like) >= 0)
            .collect(Collectors.toList());
        return accountRepo.findByBalanceAccIn(listOfInterests, page).map(x -> AccountDTO.toDTO(x));
    }

    public Page<AccountDTO> getByClient(Long clientId, Pageable page) {
        return accountRepo.findByClientClientId(clientId, page).map(x -> AccountDTO.toDTO(x));
    }

    public Optional<AccountDTO> getById(Long accId) {
        return accountRepo.findById(accId).map(x -> AccountDTO.toDTO(x));
    }

    protected String getFullAccount(BalanceAccount balanceAccount, Currency currency, Branch branch, ClientDTO clientDTO) {
        List<Account> accounts = accountRepo.getByClientAndCurrencyAndBalanceAccAndBranch(
            clientDTO.getClientId(), 
            currency,
            branch,
            balanceAccount);
        int nextIndex = 1;
        if(accounts.size() > 0) {
            nextIndex = accounts.stream()
                .map(x -> x.getFullAcc().substring(15))
                .mapToInt(Integer::valueOf)
                .max().getAsInt()+1;
        }
        return "" 
            + balanceAccount.getCode() 
            + currency.getCode() 
            + branch.getCode() 
            + String.format("%06X", clientDTO.getClientId())
            + String.format("%03d", nextIndex);
    }

    @Transactional
    public AccountCreateRespDTO createAcc(AccountCreateReqDTO reqDTO) {
        AccountCreateRespDTO resp = new AccountCreateRespDTO();
        Client client = clientRepo.findById(reqDTO.getClientId()).get();
        Branch branch = branchRepo.findById(reqDTO.getBranchCode()).get();
        User createdBy = userRepo.findByLogin(((UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).get();
        // ********************
        Account acc = new Account(
            null, 
            BalanceAccount.getByCode(reqDTO.getBalanceAcc()),
            getFullAccount(
                BalanceAccount.getByCode(reqDTO.getBalanceAcc()), 
                Currency.getByCode(reqDTO.getCurrency()), 
                branch, 
                ClientDTO.toDTO(client)), 
            Currency.getByCode(reqDTO.getCurrency()), 
            0l, 
            0l, 
            false, 
            client, 
            branch, 
            false, 
            0l, 
            0l, 
            LocalDateTime.now(), 
            createdBy);
        acc = accountRepo.save(acc);
        // ********************
        resp.setAccId(acc.getAccId());
        resp.setFullAcc(acc.getFullAcc());
        return resp;
    } 

    public void setFrozenAcc(Long accId, boolean frozen) {
        Account acc = accountRepo.findById(accId).get();
        acc.setFrozen(frozen);
        accountRepo.save(acc);
    }
}
