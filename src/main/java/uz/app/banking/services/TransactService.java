package uz.app.banking.services;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import uz.app.banking.dto.AccountDTO;
import uz.app.banking.dto.BranchDTO;
import uz.app.banking.dto.TransactInfoDTO;
import uz.app.banking.dto.TransactionDTO;
import uz.app.banking.entities.Account;
import uz.app.banking.entities.Branch;
import uz.app.banking.entities.Transaction;
import uz.app.banking.exceptions.FrozenAccountException;
import uz.app.banking.exceptions.IncorrectTransactStateException;
import uz.app.banking.exceptions.NoEntityFoundException;
import uz.app.banking.exceptions.OverdraftException;
import uz.app.banking.references.Currency;
import uz.app.banking.references.TransactState;
import uz.app.banking.repositories.AccountRepository;
import uz.app.banking.repositories.TransactionRepository;

@Service
public class TransactService {
    @Autowired
    private TransactionRepository transactRepo;

    @Autowired
    private AccountRepository accountRepo;

    @Autowired
    private UserService userService;
    
    @Autowired
    private BranchService branchService;

    public Page<TransactInfoDTO> get(
        String creditorAcc, 
        String debitorAcc, 
        String currencyCode, 
        String branchCode, 
        Long createdBy,
        Pageable page
    ) {
        /*
        String createdByStr = String.valueOf(createdBy);
        List<String> args = Stream.of(creditorAcc, debitorAcc, currencyCode, branchCode, createdByStr)
            .sequential()
            .map(x -> (x == null || x.isBlank())?null:x)
            .collect(Collectors.toList());
        return transactRepo.findByCriteria(args.get(0), args.get(1), args.get(2), args.get(3), args.get(4), page)
            .map(x -> TransactInfoDTO.toDTO(x));
        */


        return transactRepo.findByCriteria(creditorAcc, debitorAcc, currencyCode, branchCode, createdBy, page)
            .map(x -> TransactInfoDTO.toDTO(x));
    }

    @Transactional
    public TransactInfoDTO create(TransactInfoDTO transactInfoDTO) {
        AccountDTO creditor = accountRepo.findByFullAcc(transactInfoDTO.getCredit())
            .map(x -> AccountDTO.toDTO(x))
            .orElseThrow(()-> new NoEntityFoundException(transactInfoDTO.getCredit(), Account.class));
        AccountDTO debitor = accountRepo.findByFullAcc(transactInfoDTO.getDebit())
            .map(x -> AccountDTO.toDTO(x))
            .orElseThrow(()-> new NoEntityFoundException(transactInfoDTO.getDebit(), Account.class));
        if(debitor.isFrozen()) {
            throw new FrozenAccountException(debitor.getFullAcc());
        } else if(creditor.isFrozen()) {
            throw new FrozenAccountException(creditor.getFullAcc());
        } else if(!debitor.isNegativeBalanceEnabled() && debitor.getActualBalance() - transactInfoDTO.getSum() < 0) {
            throw new OverdraftException(creditor.getFullAcc(), debitor.getFullAcc(), transactInfoDTO.getSum());
        } else if(!debitor.isNegativeBalanceEnabled() && debitor.getAvailableBalance() - transactInfoDTO.getSum() < 0) {
            throw new OverdraftException(creditor.getFullAcc(), debitor.getFullAcc(), transactInfoDTO.getSum());
        }
        BranchDTO branch = branchService.getByCode(transactInfoDTO.getBranch())
            .orElseThrow(()-> new NoEntityFoundException(transactInfoDTO.getBranch(), Branch.class));
        Long sum = transactInfoDTO.getSum();//Double.valueOf(transactInfoDTO.getSum() * Math.pow(10, transactInfoDTO.getCurrency().getScale())).longValue();
        TransactionDTO tr = new TransactionDTO(
            null, 
            creditor, 
            debitor, 
            sum,
            transactInfoDTO.getCurrency(),
            transactInfoDTO.getPurpose(),
            branch,
            TransactState.NOT_APPROVED, 
            LocalDateTime.now(),
            userService.getCurrentUser().orElseThrow(()-> new RuntimeException("Illegal attempt to proceed"))
        );

        Transaction createdTransact = transactRepo.save(TransactionDTO.fromDTO(tr));
        createdTransact.getCredit().credit(createdTransact.getDebit(), createdTransact.getSum());
        accountRepo.save(createdTransact.getCredit());
        accountRepo.save(createdTransact.getDebit());
        return TransactInfoDTO.toDTO(createdTransact);
    }

    @Transactional
    public TransactInfoDTO edit(TransactInfoDTO tr) {
        if(tr.getTrId() == null)
            return tr;
        Transaction oldTr = transactRepo.findById(tr.getTrId()).orElseThrow(()->new NoEntityFoundException(tr.getTrId(), Transaction.class));
        oldTr.setPurpose(tr.getPurpose());
        return TransactInfoDTO.toDTO(transactRepo.save(oldTr));
    }

    @Transactional
    public void approve(Long transactId) {
        changeState(transactId, TransactState.APPROVED);
    }

    @Transactional
    public void cancel(Long transactId) {
        changeState(transactId, TransactState.CANCELLED);
    }

    @Transactional
    public void changeState(Long transactId, TransactState state) {
        Transaction tr = transactRepo.findById(transactId)
            .orElseThrow(()-> new NoEntityFoundException(transactId, Transaction.class));
        if(tr.getState() == state)
            return;
        switch (tr.getState()) {
            case NOT_APPROVED:
                // this is terminal state on its own branch
                throw new IncorrectTransactStateException(
                    String.format("Changing the state from %s to %s is unacceptable.", tr.getState(), state)
                );
            case APPROVED: 
                if(state == TransactState.CANCELLED) {
                    tr.setState(TransactState.CANCELLED);
                    tr.getDebit().credit(tr.getCredit(), tr.getSum());
                } else {
                    // Only cancelled state is available from this branch
                    throw new IncorrectTransactStateException(
                        String.format("Changing the state from %s to %s is unacceptable.", tr.getState(), state)
                    );
                }
                break;
            case CANCELLED:
                // this is terminal state on its own branch
                throw new IncorrectTransactStateException(
                    String.format("Changing the state from %s to %s is unacceptable.", tr.getState(), state)
                );
            case WAITING_APPROVAL:
                if(state == TransactState.APPROVED) {
                    tr.setState(TransactState.APPROVED);
                } else if(state == TransactState.CANCELLED) {
                    tr.setState(TransactState.CANCELLED);
                    tr.getDebit().credit(tr.getCredit(), tr.getSum());
                } else {
                    return;
                }
            default:
                break;
        }
        transactRepo.save(tr);
        accountRepo.save(tr.getCredit());
        accountRepo.save(tr.getDebit());
    }
}
