package uz.app.banking.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import uz.app.banking.dto.EmployeeDTO;
import uz.app.banking.repositories.EmployeeRepository;

@Service
public class EmployeeService {
    @Autowired
    private EmployeeRepository empRepo;

    public Optional<EmployeeDTO> getById(Long empId) {
        return empRepo.findById(empId).map(e -> new EmployeeDTO(e));
    }

    public Optional<EmployeeDTO> getByUserId(Long userId) {
        return empRepo.findByUserId(userId).map(e -> new EmployeeDTO(e));
    }

    public Page<EmployeeDTO> getAll(Pageable page) {
        return empRepo.findAll(page).map(e -> new EmployeeDTO(e));
    }
}
