package uz.app.banking.services;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import uz.app.banking.config.UserPoliciesConfig;
import uz.app.banking.dto.BranchDTO;
import uz.app.banking.dto.ClientDTO;
import uz.app.banking.dto.ClientSaveReqDTO;
import uz.app.banking.dto.ClientSaveRespDTO;
import uz.app.banking.dto.StandardResponseDTO;
import uz.app.banking.dto.UserDTO;
import uz.app.banking.dto.UserInfoDTO;
import uz.app.banking.entities.Client;
import uz.app.banking.entities.User;
import uz.app.banking.references.Country;
import uz.app.banking.references.DocType;
import uz.app.banking.references.UserType;
import uz.app.banking.repositories.ClientRepository;
import uz.app.banking.repositories.UserRepository;

@Service
public class ClientService {
    @Autowired
    ClientRepository clientRepo;

    @Autowired
    UserService userService;

    @Autowired
    UserPoliciesConfig userPoliciesConfig;

    @Autowired
    BranchService branchService;

    public Optional<ClientDTO> getById(Long clientId) {
        return clientRepo.findById(clientId).map(x -> ClientDTO.toDTO(x));
    }

    public Optional<ClientDTO> getByUserId(Long userId) {
        return clientRepo.findByUserUserId(userId).map(x -> ClientDTO.toDTO(x));
    }

    public Page<ClientDTO> getAll(Pageable page) {
        return clientRepo.findAll(page).map(x -> ClientDTO.toDTO(x));
    }

    public UserDTO getNewUserForClient(ClientDTO clientDTO) {
        UserInfoDTO currentUserDTO = userService.getCurrentUser().orElseThrow();
        UserInfoDTO userInfoDTO = new UserInfoDTO();
        userInfoDTO.setLogin(userPoliciesConfig.defaultClientLogin(clientDTO));
        userInfoDTO.setUserType(UserType.CLIENT);
        userInfoDTO.setCreatedAt(LocalDateTime.now());
        userInfoDTO.setCreatedBy(currentUserDTO);
        UserDTO userDTO = new UserDTO(userInfoDTO, userPoliciesConfig.defaultClientPassword());
        return userDTO;
    }

    public ClientSaveRespDTO create(ClientSaveReqDTO clientSaveReqDTO) throws Exception {
        Client client;
        ClientDTO clientDTO = new ClientDTO(
            clientSaveReqDTO.getClientId(), 
            clientSaveReqDTO.getFirstName(), 
            clientSaveReqDTO.getLastName(), 
            clientSaveReqDTO.getMiddleName(), 
            Country.getByCode(clientSaveReqDTO.getResidency()), 
            DocType.getByCode(clientSaveReqDTO.getDocType()), 
            clientSaveReqDTO.getDocSeria(), 
            clientSaveReqDTO.getDocNumber(), 
            branchService.getByCode(clientSaveReqDTO.getBranch()).get(), 
            null , null, null);
        client = ClientDTO.fromDTO(clientDTO);
        UserDetails currentUser = (UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        client.setCreatedAt(LocalDateTime.now());
        client.setCreatedBy(UserDTO.fromDTO(userService.getUserByLogin(currentUser.getUsername()).get()));
        UserDTO clientUser = getNewUserForClient(clientDTO);
        UserInfoDTO createdUser = userService.save(clientUser);

        client.setUser(UserInfoDTO.fromDTO(createdUser));    
        client = clientRepo.save(client);
        
        return new ClientSaveRespDTO(client.getClientId(), clientUser.getLogin(), clientUser.getPassword());
    }

    public StandardResponseDTO update(ClientSaveReqDTO clientSaveReqDTO) {
        Optional<Client> opClient = clientRepo.findById(clientSaveReqDTO.getClientId());
        if(!opClient.isPresent()) {
            return new StandardResponseDTO("1", "No such client found");
        }
        Client client = opClient.get();
        client.setFirstName(clientSaveReqDTO.getFirstName());
        client.setLastName(clientSaveReqDTO.getLastName());
        client.setMiddleName(clientSaveReqDTO.getMiddleName());
        client.setResidency(Country.getByCode(clientSaveReqDTO.getResidency()));
        client.setDocType(DocType.getByCode(clientSaveReqDTO.getDocType()));
        client.setDocSeria(clientSaveReqDTO.getDocSeria());
        client.setDocNumber(clientSaveReqDTO.getDocNumber());
        client = clientRepo.save(client);
        return new StandardResponseDTO("0", "Success");
        //return new ClientSaveRespDTO(client.getClientId(), client.getUser().getLogin(), "");
    }
}
