package uz.app.banking.dto;

import java.time.LocalDateTime;

import uz.app.banking.entities.User;
import uz.app.banking.references.UserType;

public class UserInfoDTO {
    private Long id;
    private String login;
    private UserType userType;
    private LocalDateTime createdAt;
    private UserInfoDTO createdBy;
    
    public UserInfoDTO() {};

    public UserInfoDTO(UserInfoDTO clone) {
        this.id = clone.id;
        this.login = clone.login;
        this.userType = clone.userType;
        this.createdAt = clone.createdAt;
        this.createdBy = clone.createdBy;
    }

    public static UserInfoDTO toDTO(User user) {
        if(user == null) return null;
        UserInfoDTO dto = new UserInfoDTO();
        dto.id = user.getUserId();
        dto.login = user.getLogin();
        dto.userType = user.getUserType();
        dto.createdAt = user.getCreatedAt();
        dto.createdBy = UserInfoDTO.toDTO(user.getCreatedBy());
        return dto;
    }

    public static User fromDTO(UserInfoDTO userInfoDTO) {
        if(userInfoDTO == null) return null;
        User user = new User(
            userInfoDTO.getId(), 
            userInfoDTO.getLogin(), 
            null, 
            userInfoDTO.getUserType(), 
            userInfoDTO.getCreatedAt(), 
            fromDTO(userInfoDTO.getCreatedBy()));
        return user;
    }

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public UserType getUserType() {
        return userType;
    }
    public void setUserType(UserType userType) {
        this.userType = userType;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
    public UserInfoDTO getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(UserInfoDTO createdBy) {
        this.createdBy = createdBy;
    }
}
