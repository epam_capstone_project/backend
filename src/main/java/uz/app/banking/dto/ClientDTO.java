package uz.app.banking.dto;

import java.time.LocalDateTime;

import uz.app.banking.entities.Client;
import uz.app.banking.references.Country;
import uz.app.banking.references.DocType;

public class ClientDTO {
    private Long clientId;
    private String firstName;
    private String lastName;
    private String middleName;
    private Country residency;
    private DocType docType;
    private String docSeria;
    private String docNumber;
    private BranchDTO branch;
    private UserInfoDTO user;
    private LocalDateTime createdAt;
    private UserInfoDTO createdBy;

    public static ClientDTO toDTO(Client client) {
        if(client == null) return null;
        ClientDTO dto = new ClientDTO();
        dto.clientId = client.getClientId();
        dto.firstName = client.getFirstName();
        dto.lastName = client.getLastName();
        dto.middleName = client.getMiddleName();
        dto.residency = client.getResidency();
        dto.docType = client.getDocType();
        dto.docSeria = client.getDocSeria();
        dto.docNumber = client.getDocNumber();
        dto.branch = BranchDTO.toDTO(client.getBranch());
        dto.user = UserInfoDTO.toDTO(client.getUser());
        dto.createdAt = client.getCreatedAt();
        dto.createdBy = UserInfoDTO.toDTO(client.getCreatedBy());
        return dto;
    }
    public static Client fromDTO(ClientDTO clientDTO) {
        if(clientDTO == null) return null;
        Client client = new Client(
            clientDTO.getClientId(),
            clientDTO.getFirstName(), 
            clientDTO.getLastName(), 
            clientDTO.getMiddleName(), 
            clientDTO.getResidency(), 
            clientDTO.getDocType(), 
            clientDTO.getDocSeria(), 
            clientDTO.getDocNumber(), 
            BranchDTO.fromDTO(clientDTO.getBranch()),
            UserInfoDTO.fromDTO(clientDTO.getUser()),  
            clientDTO.getCreatedAt(),
            UserDTO.fromDTO(clientDTO.getCreatedBy()));
        return client;
    }

    public ClientDTO() {
    }
    public ClientDTO(Long clientId, String firstName, String lastName, String middleName, Country residency,
            DocType docType, String docSeria, String docNumber, BranchDTO branch, UserInfoDTO user, LocalDateTime createdAt,
            UserInfoDTO createdBy) {
        this.clientId = clientId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.residency = residency;
        this.docType = docType;
        this.docSeria = docSeria;
        this.docNumber = docNumber;
        this.branch = branch;
        this.user = user;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
    }
    public Long getClientId() {
        return clientId;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getMiddleName() {
        return middleName;
    }
    public Country getResidency() {
        return residency;
    }
    public DocType getDocType() {
        return docType;
    }
    public String getDocSeria() {
        return docSeria;
    }
    public String getDocNumber() {
        return docNumber;
    }
    public BranchDTO getBranch() {
        return branch;
    }
    public UserInfoDTO getUser() {
        return user;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public UserInfoDTO getCreatedBy() {
        return createdBy;
    }

    
}
