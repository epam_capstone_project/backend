package uz.app.banking.dto;

public class ClientSaveReqDTO {
    private Long clientId;
    private String firstName;
    private String lastName;
    private String middleName;
    private String residency;
    private String docType;
    private String docSeria;
    private String docNumber;
    private String branch;

    public ClientSaveReqDTO() {
    }
    public Long getClientId() {
        return clientId;
    }
    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getMiddleName() {
        return middleName;
    }
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    public String getResidency() {
        return residency;
    }
    public void setResidency(String residency) {
        this.residency = residency;
    }
    public String getDocType() {
        return docType;
    }
    public void setDocType(String docType) {
        this.docType = docType;
    }
    public String getDocSeria() {
        return docSeria;
    }
    public void setDocSeria(String docSeria) {
        this.docSeria = docSeria;
    }
    public String getDocNumber() {
        return docNumber;
    }
    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }
    public String getBranch() {
        return branch;
    }
    public void setBranch(String branch) {
        this.branch = branch;
    }
}
