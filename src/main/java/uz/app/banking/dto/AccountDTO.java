package uz.app.banking.dto;

import java.time.LocalDateTime;

import uz.app.banking.entities.Account;
import uz.app.banking.references.BalanceAccount;
import uz.app.banking.references.Currency;

public class AccountDTO {
    private Long accId;
    private BalanceAccount balanceAcc;
    private String fullAcc;
    private Currency currency;
    private Long availableBalance;
    private Long actualBalance;
    private boolean negativeBalanceEnabled;
    private ClientDTO client;
    private BranchDTO branch;
    private boolean frozen;
    private Long turnoverAllDebit;
    private Long turnoverAllCredit;
    private LocalDateTime createdAt;
    private UserInfoDTO createdBy;
    public Long getAccId() {
        return accId;
    }
    public void setAccId(Long accId) {
        this.accId = accId;
    }
    public BalanceAccount getBalanceAcc() {
        return balanceAcc;
    }
    public void setBalanceAcc(BalanceAccount balanceAcc) {
        this.balanceAcc = balanceAcc;
    }
    public String getFullAcc() {
        return fullAcc;
    }
    public void setFullAcc(String fullAcc) {
        this.fullAcc = fullAcc;
    }
    public Currency getCurrency() {
        return currency;
    }
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    public Long getAvailableBalance() {
        return availableBalance;
    }
    public void setAvailableBalance(Long availableBalance) {
        this.availableBalance = availableBalance;
    }
    public Long getActualBalance() {
        return actualBalance;
    }
    public void setActualBalance(Long actualBalance) {
        this.actualBalance = actualBalance;
    }
    public boolean isNegativeBalanceEnabled() {
        return negativeBalanceEnabled;
    }
    public void setNegativeBalanceEnabled(boolean negativeBalanceEnabled) {
        this.negativeBalanceEnabled = negativeBalanceEnabled;
    }
    public ClientDTO getClient() {
        return client;
    }
    public void setClient(ClientDTO client) {
        this.client = client;
    }
    public BranchDTO getBranch() {
        return branch;
    }
    public void setBranch(BranchDTO branch) {
        this.branch = branch;
    }
    public boolean isFrozen() {
        return frozen;
    }
    public void setFrozen(boolean frozen) {
        this.frozen = frozen;
    }
    public Long getTurnoverAllDebit() {
        return turnoverAllDebit;
    }
    public void setTurnoverAllDebit(Long turnoverAllDebit) {
        this.turnoverAllDebit = turnoverAllDebit;
    }
    public Long getTurnoverAllCredit() {
        return turnoverAllCredit;
    }
    public void setTurnoverAllCredit(Long turnoverAllCredit) {
        this.turnoverAllCredit = turnoverAllCredit;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
    public UserInfoDTO getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(UserInfoDTO createdBy) {
        this.createdBy = createdBy;
    }
    public AccountDTO() {
    }

    public static AccountDTO toDTO(Account acc) {
        if(acc == null) return null;
        AccountDTO accountDTO = new AccountDTO();
        accountDTO.accId = acc.getAccId();
        accountDTO.balanceAcc = acc.getBalanceAcc();
        accountDTO.fullAcc = acc.getFullAcc();
        accountDTO.currency = acc.getCurrency();
        accountDTO.availableBalance = acc.getAvailableBalance();
        accountDTO.actualBalance = acc.getActualBalance();
        accountDTO.negativeBalanceEnabled = acc.isNegativeBalanceEnabled();
        accountDTO.client = ClientDTO.toDTO(acc.getClient());
        accountDTO.branch = BranchDTO.toDTO(acc.getBranch());
        accountDTO.frozen = acc.isFrozen();
        accountDTO.turnoverAllDebit = acc.getTurnoverAllDebit();
        accountDTO.turnoverAllCredit = acc.getTurnoverAllCredit();
        accountDTO.createdAt = acc.getCreatedAt();
        accountDTO.createdBy = UserInfoDTO.toDTO(acc.getCreatedBy());
        return accountDTO;
    }
    
    public static Account fromDTO(AccountDTO dto) {
        if(dto == null) return null;
        Account acc = new Account(
            dto.accId, 
            dto.balanceAcc, 
            dto.fullAcc, 
            dto.currency, 
            dto.availableBalance, 
            dto.actualBalance, 
            dto.negativeBalanceEnabled, 
            ClientDTO.fromDTO(dto.client), 
            BranchDTO.fromDTO(dto.branch), 
            dto.frozen, 
            dto.turnoverAllDebit, 
            dto.turnoverAllCredit, 
            dto.createdAt, 
            UserInfoDTO.fromDTO(dto.createdBy)
        );
        return acc;
    }
}
