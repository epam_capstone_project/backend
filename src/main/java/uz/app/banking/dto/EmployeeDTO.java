package uz.app.banking.dto;

import java.time.LocalDateTime;

import uz.app.banking.entities.Employee;

public class EmployeeDTO {
    private Long empId;
    private String firstName;
    private String lastName;
    private String middleName;
    private BranchDTO branch;
    private String department;
    private LocalDateTime createdAt;
    private UserInfoDTO createdBy;
    private Long userId;

    public EmployeeDTO(Employee emp) {
        this.empId = emp.getEmpId();
        this.firstName = emp.getFirstName();
        this.lastName = emp.getLastName();
        this.middleName = emp.getMiddleName();
        this.branch = BranchDTO.toDTO(emp.getBranch());
        this.department = emp.getDepartment();
        this.createdAt = emp.getCreatedAt();
        this.createdBy = UserInfoDTO.toDTO(emp.getCreatedBy());
        this.userId = emp.getUserId();
    }

    public EmployeeDTO(Long empId, String firstName, String lastName, String middleName, BranchDTO branch,
            String department, LocalDateTime createdAt, UserInfoDTO createdBy, Long userId) {
        this.empId = empId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.branch = branch;
        this.department = department;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.userId = userId;
    }
    public Long getEmpId() {
        return empId;
    }
    public String getFirstName() {
        return firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public String getMiddleName() {
        return middleName;
    }
    public BranchDTO getBranch() {
        return branch;
    }
    public String getDepartment() {
        return department;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public UserInfoDTO getCreatedBy() {
        return createdBy;
    }
    public Long getUserId() {
        return userId;
    }
}
