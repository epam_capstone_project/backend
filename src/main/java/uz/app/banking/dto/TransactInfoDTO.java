package uz.app.banking.dto;

import java.time.LocalDateTime;

import uz.app.banking.entities.Transaction;
import uz.app.banking.references.Currency;
import uz.app.banking.references.TransactState;

public class TransactInfoDTO {
    private Long trId;
    private String credit;
    private String debit;
    private Long sum;
    private Currency currency;
    private String purpose;
    private String branch;
    private TransactState state;
    private LocalDateTime createdAt;
    private String createdBy;
    public Long getTrId() {
        return trId;
    }
    public void setTrId(Long trId) {
        this.trId = trId;
    }
    public String getCredit() {
        return credit;
    }
    public void setCredit(String creditorAccCode) {
        this.credit = creditorAccCode;
    }
    public String getDebit() {
        return debit;
    }
    public void setDebit(String debitorAccCode) {
        this.debit = debitorAccCode;
    }
    public Long getSum() {
        return sum;
    }
    public void setSum(Long sum) {
        this.sum = sum;
    }
    public Currency getCurrency() {
        return currency;
    }
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    public String getPurpose() {
        return purpose;
    }
    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }
    public String getBranch() {
        return branch;
    }
    public void setBranch(String branch) {
        this.branch = branch;
    }
    public TransactState getState() {
        return state;
    }
    public void setState(TransactState state) {
        this.state = state;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
    public String getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
    public TransactInfoDTO() {
    }

    public static TransactInfoDTO toDTO(Transaction t) {
        if(t == null) return null;
        TransactInfoDTO dto = new TransactInfoDTO();
        dto.trId = t.getTrId();
        dto.credit = t.getCredit().getFullAcc();
        dto.debit = t.getDebit().getFullAcc();
        dto.sum = t.getSum();
        dto.currency = t.getCurrency();
        dto.purpose = t.getPurpose();
        dto.branch = t.getBranch().getCode();
        dto.state = t.getState();
        dto.createdAt = t.getCreatedAt();
        dto.createdBy = null;
        return dto;
    }
}
