package uz.app.banking.dto;

public class UserDTO extends UserInfoDTO {
    private String password;
    private String passwordConfirm;
    
    public UserDTO() {};

    /*public static UserDTO toDTO(User user) {
        if(user == null) return null;
        UserDTO dto = new UserDTO();
        dto.id = user.getUserId();
        dto.login = user.getLogin();
        dto.userType = user.getUserType();
        dto.createdAt = user.getCreatedAt();
        dto.createdBy = UserInfoDTO.toDTO(user.getCreatedBy());
    }*/

    public UserDTO(UserInfoDTO userInfoDTO, String password, String passwordConfirm) {
        super(userInfoDTO);
        this.password = password;
        this.passwordConfirm = passwordConfirm;
    }

    public UserDTO(UserInfoDTO userInfoDTO, String password) {
        super(userInfoDTO);
        this.password = password;
        this.passwordConfirm = password;
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getPasswordConfirm() {
        return passwordConfirm;
    }
    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }
}
