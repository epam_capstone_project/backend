package uz.app.banking.dto;

import java.time.LocalDateTime;

import uz.app.banking.entities.Transaction;
import uz.app.banking.references.Currency;
import uz.app.banking.references.TransactState;

public class TransactionDTO {
    private Long trId;
    private AccountDTO creditor;
    private AccountDTO debitor;
    private Long sum;
    private Currency currency;
    private String purpose;
    private BranchDTO branch;
    private TransactState state;
    private LocalDateTime createdAt;
    private UserInfoDTO createdBy;
    public Long getTrId() {
        return trId;
    }
    public void setTrId(Long trId) {
        this.trId = trId;
    }
    public AccountDTO getCreditor() {
        return creditor;
    }
    public void setCreditor(AccountDTO creditor) {
        this.creditor = creditor;
    }
    public AccountDTO getDebitor() {
        return debitor;
    }
    public void setDebitor(AccountDTO debitor) {
        this.debitor = debitor;
    }
    public Long getSum() {
        return sum;
    }
    public void setSum(Long sum) {
        this.sum = sum;
    }
    public Currency getCurrency() {
        return currency;
    }
    public void setCurrency(Currency currency) {
        this.currency = currency;
    }
    public String getPurpose() {
        return purpose;
    }
    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }
    public BranchDTO getBranch() {
        return branch;
    }
    public void setBranch(BranchDTO branch) {
        this.branch = branch;
    }
    public TransactState getState() {
        return state;
    }
    public void setState(TransactState state) {
        this.state = state;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
    public UserInfoDTO getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(UserInfoDTO createdBy) {
        this.createdBy = createdBy;
    }
    public TransactionDTO() {
    }
    public TransactionDTO(Long trId, AccountDTO creditor, AccountDTO debitor, Long sum, Currency currency,
            String purpose, BranchDTO branch, TransactState state, LocalDateTime createdAt, UserInfoDTO createdBy) {
        this.trId = trId;
        this.creditor = creditor;
        this.debitor = debitor;
        this.sum = sum;
        this.currency = currency;
        this.purpose = purpose;
        this.branch = branch;
        this.state = state;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
    }

    public static Transaction fromDTO(TransactionDTO dto) {
        return new Transaction(
            dto.trId, 
            AccountDTO.fromDTO(dto.creditor),
            AccountDTO.fromDTO(dto.debitor), 
            dto.sum, 
            dto.currency, 
            dto.purpose, 
            BranchDTO.fromDTO(dto.branch), 
            dto.state, 
            dto.createdAt, 
            UserInfoDTO.fromDTO(dto.createdBy)
        );
    }
}
