package uz.app.banking.dto;

public class ClientSaveRespDTO {
    private Long clientID;
    private String login;
    private String password;

    public ClientSaveRespDTO() {
    }

    public ClientSaveRespDTO(Long clientId, String login, String password) {
        this.clientID = clientId;
        this.login = login;
        this.password = password;
    }

    // public ClientSaveRespDTO(ClientDTO clientDTO) {
    //     this.clientID = clientDTO.getClientId();
    //     this.login = clientDTO.getUser().getLogin();
    // }
    public Long getClientID() {
        return clientID;
    }
    public void setClientID(Long clientID) {
        this.clientID = clientID;
    }
    public String getLogin() {
        return login;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    
}
