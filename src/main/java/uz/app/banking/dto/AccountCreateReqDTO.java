package uz.app.banking.dto;

import java.time.LocalDateTime;

import uz.app.banking.references.BalanceAccount;
import uz.app.banking.references.Currency;

public class AccountCreateReqDTO {
    private String balanceAcc;
    private String currency;
    private boolean negativeBalanceEnabled;
    private Long clientId;
    private String branchCode;
    private boolean frozen;
    public String getBalanceAcc() {
        return balanceAcc;
    }
    public void setBalanceAcc(String balanceAcc) {
        this.balanceAcc = balanceAcc;
    }
    public String getCurrency() {
        return currency;
    }
    public void setCurrency(String currency) {
        this.currency = currency;
    }
    public boolean isNegativeBalanceEnabled() {
        return negativeBalanceEnabled;
    }
    public void setNegativeBalanceEnabled(boolean negativeBalanceEnabled) {
        this.negativeBalanceEnabled = negativeBalanceEnabled;
    }
    public Long getClientId() {
        return clientId;
    }
    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }
    public String getBranchCode() {
        return branchCode;
    }
    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }
    public boolean isFrozen() {
        return frozen;
    }
    public void setFrozen(boolean frozen) {
        this.frozen = frozen;
    }
    public AccountCreateReqDTO() {
    }
}
