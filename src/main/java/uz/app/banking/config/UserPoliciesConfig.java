package uz.app.banking.config;

import java.util.Base64;
import java.util.Random;
import java.util.UUID;

import org.springframework.context.annotation.Configuration;

import uz.app.banking.dto.ClientDTO;
import uz.app.banking.dto.EmployeeDTO;

@Configuration
public class UserPoliciesConfig {
    private Random rand = new Random(System.currentTimeMillis());

    public String generateRandomStr(int maxLength) {
        byte[] randomBytes = new byte[(maxLength + 1) * 6 / 8];
        rand.nextBytes(randomBytes);
        return Base64.getEncoder().encodeToString(randomBytes);
    }

    public String defaultEmployeeLogin(EmployeeDTO employeeDTO) {
        return "" 
            + employeeDTO.getFirstName().toUpperCase().subSequence(0, 1)
            + employeeDTO.getLastName().toUpperCase().subSequence(0, 1) 
            + employeeDTO.getMiddleName().toUpperCase().subSequence(0, 1) 
            + "_" + employeeDTO.getBranch().getCode()
            + "_" + generateRandomStr(4)
            ;
    }

    public String defaultEmployeePassword() {
        return generateRandomStr(10);
    }

    public String defaultClientLogin(ClientDTO clientDTO) {
        return "" 
            + clientDTO.getFirstName().toUpperCase().subSequence(0, 1)
            + clientDTO.getLastName().toUpperCase().subSequence(0, 1) 
            + clientDTO.getMiddleName().toUpperCase().subSequence(0, 1) 
            + "_" + clientDTO.getBranch().getCode()
            + "_" + generateRandomStr(4)
            ;
    }

    public String defaultClientPassword() {
        return generateRandomStr(10);
    }
}
