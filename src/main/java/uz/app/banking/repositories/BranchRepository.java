package uz.app.banking.repositories;

import org.springframework.data.repository.PagingAndSortingRepository;

import uz.app.banking.entities.Branch;

public interface BranchRepository extends PagingAndSortingRepository<Branch, String> {
    
}
