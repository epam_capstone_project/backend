package uz.app.banking.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import uz.app.banking.entities.Branch;
import uz.app.banking.entities.Client;
import uz.app.banking.references.DocType;

public interface ClientRepository extends PagingAndSortingRepository<Client, Long> {
    public Optional<Client> findByUserUserId(Long userId);

    public List<Client> findByDocTypeAndDocSeriaAndDocNumber(DocType docType, String docSeria, String docNumber);

    public Long countByDocTypeAndDocSeriaAndDocNumber(DocType docType, String docSeria, String docNumber);
}
