package uz.app.banking.repositories;


import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import uz.app.banking.entities.Account;
import uz.app.banking.entities.Branch;
import uz.app.banking.references.BalanceAccount;
import uz.app.banking.references.Currency;

public interface AccountRepository extends PagingAndSortingRepository<Account, Long> {
    public Page<Account> findByClientClientId(Long clientId, Pageable page);

    public Page<Account> findByCurrencyCodeLike(String currencyCode, Pageable page);

    // @Query("select acc from Accounts acc where acc.balanceAcc.getCode() like ?1")
    // public Page<Account> findByBalanceAccLike(String balanceAcc, Pageable page);

    public Page<Account> findByBalanceAccIn(Collection<BalanceAccount> balanceAcc, Pageable page);

    // @Query("select count(acc) from Account acc where acc.client.id=?1 and acc.")
    // public Long count(Long clientId, Branch branch, Currency currency);
    
    //public Long countByClientClientIdAndBranchAndCurrency(Long clientId, Branch branch, Currency currency);

    @Query("select acc from Accounts acc where acc.client.id = ?1 and acc.currency = ?2 and acc.branch = ?3 and acc.balanceAcc = ?4")
    public List<Account> getByClientAndCurrencyAndBalanceAccAndBranch(Long clientId, Currency currency, Branch branch, BalanceAccount balanceAcc);

    public Page<Account> findByCurrencyIn(Collection<Currency> currencies, Pageable page);

    public Optional<Account> findByFullAcc(String fullAcc);
}
