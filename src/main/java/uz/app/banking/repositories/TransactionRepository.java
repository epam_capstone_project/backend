package uz.app.banking.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import uz.app.banking.entities.Transaction;

public interface TransactionRepository extends PagingAndSortingRepository<Transaction, Long> {
    @Query("select t from Transaction t where "
    + "    (:credit is null or t.credit.fullAcc like :credit) "
    + "and (:debit is null or t.debit.fullAcc like :debit) "
    + "and (:currencyCode is null or t.currency = :currencyCode) "
    + "and (:branchCode is null or t.branch.code = :branchCode) "
    + "and (:createdBy is null or t.createdBy.userId = :createdBy)")
    public Page<Transaction> findByCriteria(
        String credit, 
        String debit, 
        String currencyCode, 
        String branchCode, 
        Long createdBy,
        Pageable page
    );
}
