package uz.app.banking.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.boot.autoconfigure.data.web.SpringDataWebProperties.Pageable;
import org.springframework.data.domain.Page;
import org.springframework.data.repository.PagingAndSortingRepository;

import uz.app.banking.entities.User;

public interface UserRepository extends PagingAndSortingRepository<User, Long> {
    public Optional<User> findByLogin(String login);

    public Optional<User> findById(Long id);
    
    @SuppressWarnings("unchecked")
    public User save(User user);
}
