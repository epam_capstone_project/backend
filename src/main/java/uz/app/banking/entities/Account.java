package uz.app.banking.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import uz.app.banking.references.BalanceAccount;
import uz.app.banking.references.Currency;

@Entity(name = "Accounts")
@Table(name = "accounts", schema = "public")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "acc_id")
    private Long accId;

    @Column(name = "balance_acc")
    private BalanceAccount balanceAcc;

    @Column(name = "full_acc", nullable = false, unique = true, updatable = false)
    private String fullAcc;

    @Column(name = "currency")
    private Currency currency;

    @Column(name = "available_balance")
    private volatile Long availableBalance;

    @Column(name = "actual_balance")
    private volatile Long actualBalance;

    @Column(name = "negative_balance_enabled")
    private boolean negativeBalanceEnabled;

    @JoinColumn(name = "client_id", referencedColumnName = "client_id")
    @ManyToOne
    private Client client;

    @JoinColumn(name = "branch_code"  )
    @ManyToOne
    private Branch branch;

    @Column(name = "frozen")
    private boolean frozen;

    @Column(name = "turnover_all_debit")
    private Long turnoverAllDebit;

    @Column(name = "turnover_all_credit")
    private Long turnoverAllCredit;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @JoinColumn(name = "created_by", referencedColumnName = "user_id")
    @ManyToOne
    private User createdBy;

    public Long getAccId() {
        return accId;
    }
    public BalanceAccount getBalanceAcc() {
        return balanceAcc;
    }
    public String getFullAcc() {
        return fullAcc;
    }
    public Currency getCurrency() {
        return currency;
    }
    public Long getAvailableBalance() {
        return availableBalance;
    }
    public void setAvailableBalance(Long availableBalance) {
        this.availableBalance = availableBalance;
    }
    public Long getActualBalance() {
        return actualBalance;
    }
    public void setActualBalance(Long actualBalance) {
        this.actualBalance = actualBalance;
    }
    public boolean isNegativeBalanceEnabled() {
        return negativeBalanceEnabled;
    }
    public void setNegativeBalanceEnabled(boolean negativeBalanceEnabled) {
        this.negativeBalanceEnabled = negativeBalanceEnabled;
    }
    public Client getClient() {
        return client;
    }
    public Branch getBranch() {
        return branch;
    }
    public boolean isFrozen() {
        return frozen;
    }
    public void setFrozen(boolean frozen) {
        this.frozen = frozen;
    }
    public Long getTurnoverAllDebit() {
        return turnoverAllDebit;
    }
    public void setTurnoverAllDebit(Long turnoverAllDebit) {
        this.turnoverAllDebit = turnoverAllDebit;
    }
    public Long getTurnoverAllCredit() {
        return turnoverAllCredit;
    }
    public void setTurnoverAllCredit(Long turnoverAllCredit) {
        this.turnoverAllCredit = turnoverAllCredit;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public User getCreatedBy() {
        return createdBy;
    }
    public Account(Long accId, BalanceAccount balanceAcc, String fullAcc, Currency currency, Long availableBalance,
            Long actualBalance, boolean negativeBalanceEnabled, Client client, Branch branch, boolean frozen,
            Long turnoverAllDebit, Long turnoverAllCredit, LocalDateTime createdAt, User createdBy) {
        this.accId = accId;
        this.balanceAcc = balanceAcc;
        this.fullAcc = fullAcc;
        this.currency = currency;
        this.availableBalance = availableBalance;
        this.actualBalance = actualBalance;
        this.negativeBalanceEnabled = negativeBalanceEnabled;
        this.client = client;
        this.branch = branch;
        this.frozen = frozen;
        this.turnoverAllDebit = turnoverAllDebit;
        this.turnoverAllCredit = turnoverAllCredit;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
    }
    public Account() {
    }

    public synchronized void debit(Account creditAcc, Long sum) {
        this.availableBalance -= sum;
        this.actualBalance -= sum;

        creditAcc.availableBalance += sum;
        creditAcc.actualBalance += sum;
    }

    public synchronized void credit(Account debitAcc, Long sum) {
        this.availableBalance += sum;
        this.actualBalance += sum;

        debitAcc.availableBalance -= sum;
        debitAcc.actualBalance -= sum;
    }
}
