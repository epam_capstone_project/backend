package uz.app.banking.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name = "branches")
@Table(schema = "public", name = "branches")
public class Branch {
    @Id
    @Column(name = "code", unique = true)
    private String code;
    @Column(name = "name")
    private String name;
    @Column(name = "region")
    private String region;
    
    public Branch() {
    }
    public Branch(String code, String name, String region) {
        this.code = code;
        this.name = name;
        this.region = region;
    }

    public String getCode() {
        return code;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getRegion() {
        return region;
    }
    public void setRegion(String region) {
        this.region = region;
    }
}
