package uz.app.banking.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import uz.app.banking.references.Currency;
import uz.app.banking.references.TransactState;

@Entity(name="Transaction")
@Table(name = "transactions", schema = "public")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="trId")
    private Long trId;
    
    @JoinColumn(name = "credit_acc", referencedColumnName = "acc_id")
    @ManyToOne
    private Account credit;

    @JoinColumn(name = "debit_acc", referencedColumnName = "acc_id")
    @ManyToOne
    private Account debit;

    @Column(name = "sum")
    private Long sum;

    @Column(name = "currency")
    private Currency currency;

    @Column(name = "purpose")
    private String purpose;

    @JoinColumn(name = "branch_code")
    @ManyToOne
    private Branch branch;

    @Column(name = "state")
    private TransactState state;

    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @JoinColumn(name = "created_by")
    @ManyToOne
    private User createdBy;

    public Long getTrId() {
        return trId;
    }
    public Account getCredit() {
        return credit;
    }
    public Account getDebit() {
        return debit;
    }
    public Long getSum() {
        return sum;
    }
    public Currency getCurrency() {
        return currency;
    }
    public String getPurpose() {
        return purpose;
    }
    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }
    public Branch getBranch() {
        return branch;
    }
    public TransactState getState() {
        return state;
    }
    public void setState(TransactState state) {
        this.state = state;
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public User getCreatedBy() {
        return createdBy;
    }

    
    public Transaction() {
    }
    public Transaction(Long trId, Account creditor, Account debitor, Long sum, Currency currency, String purpose,
            Branch branch, TransactState state, LocalDateTime createdAt, User createdBy) {
        this.trId = trId;
        this.credit = creditor;
        this.debit = debitor;
        this.sum = sum;
        this.currency = currency;
        this.purpose = purpose;
        this.branch = branch;
        this.state = state;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
    }

    
}
