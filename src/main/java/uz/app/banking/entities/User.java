package uz.app.banking.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import uz.app.banking.references.UserType;

@Entity(name = "Users")
@Table(name = "Users", schema = "public")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long userId;
    @Column(name = "login", unique = true)
    private String login;
    @Column(name = "password_hash")
    private String passwordHash;
    @Column(name = "user_type")
    private Integer userType;
    @Column(name = "created_at")
    private LocalDateTime createdAt;
    @JoinColumn(name = "created_by")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;

    
    public User(){};

    public User(Long userId, String login, String passwordHash, UserType userType, LocalDateTime createdAt,
            User createdBy) {
        this.userId = userId;
        this.login = login;
        this.passwordHash = passwordHash;
        this.userType = userType.getCode();
        this.createdAt = createdAt;
        this.createdBy = createdBy;
    }
    public Long getUserId() {
        return userId;
    }
    public String getLogin() {
        return login;
    }
    public String getPasswordHash() {
        return passwordHash;
    }
    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }
    public UserType getUserType() {
        return UserType.getByCode(this.userType);
    }
    public LocalDateTime getCreatedAt() {
        return createdAt;
    }
    public User getCreatedBy() {
        return createdBy;
    }

    
}
