package uz.app.banking.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import uz.app.banking.references.Country;
import uz.app.banking.references.DocType;

@Entity(name="clients")
@Table(schema = "public", name = "clients")
public class Client {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "client_id")
    private Long clientId;

    @Column(name = "first_name")
    private String firstName;
    
    @Column(name = "last_name")
    private String lastName;
    
    @Column(name = "middle_name")
    private String middleName;
    
    @Column(name = "residency")
    //@Type(type = "uz.app.banking.adapters.CountryTypeAdapter")
    private Country residency;
    
    @Column(name = "doc_type")
    //@Type(type = "uz.app.banking.adapters.DocTypeTypeAdapter")
    private DocType docType;
    
    @Column(name = "doc_seria")
    private String docSeria;
    
    @Column(name = "doc_number")
    private String docNumber;
    
    @JoinColumn(name = "branch_code")
    @ManyToOne(fetch = FetchType.LAZY)
    private Branch branch;
    
    // @Column(name = "user_id")
    // private Long userId;
    
    @JoinColumn(name = "user_id", referencedColumnName = "user_id")
    @OneToOne(fetch = FetchType.LAZY)
    private User user;
    

    @Column(name = "created_at")
    private LocalDateTime createdAt;
    
    @JoinColumn(name = "created_by")
    @ManyToOne(fetch = FetchType.LAZY)
    private User createdBy;

    public Client(){}
    
    public Client(Long clientId, String firstName, String lastName, String middleName, Country residency,
            DocType docType, String docSeria, String docNumber, Branch branch, User user, LocalDateTime createdAt,
            User createdBy) {
        this.clientId = clientId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
        this.residency = residency;
        this.docType = docType;
        this.docSeria = docSeria;
        this.docNumber = docNumber;
        this.branch = branch;
        this.user = user;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
    }

    public Long getClientId() {
        return clientId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Country getResidency() {
        return residency;
    }

    public void setResidency(Country residency) {
        this.residency = residency;
    }

    public DocType getDocType() {
        return docType;
    }

    public void setDocType(DocType docType) {
        this.docType = docType;
    }

    public String getDocSeria() {
        return docSeria;
    }

    public void setDocSeria(String docSeria) {
        this.docSeria = docSeria;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public User getUser() {
        return user;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }
}
